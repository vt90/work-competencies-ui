import React, { Fragment } from 'react';
import DeleteIcon from '@mui/icons-material/Delete';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import { IUser } from '../../../models/user';

interface IUsersListProps {
  users: IUser[];
  onDeleteUser?: (user: IUser) => void;
  onSelectUser?: (user: IUser) => void;
}

interface IUsersListItemProps {
  isLastItem: boolean;
  user: IUser;
  onDeleteUser?: (user: IUser) => void;
  onSelectUser?: (user: IUser) => void;
}

const UserListItem = (props: IUsersListItemProps) => {
  const { isLastItem, user, onSelectUser, onDeleteUser } = props;

  const { user_id, email, picture, name } = user;
  const avatarProps = {
    children: name.charAt(0).toUpperCase(),
    ...(picture && { src: picture }),
  };
  const textProps = {
    primary: name,
    ...(email !== name && { secondary: email }),
  };

  const ListItemContainer = onSelectUser ? ListItemButton : Fragment;

  return (
    <ListItem
      key={user_id}
      onClick={() => onSelectUser && onSelectUser(user)}
      divider={!isLastItem}
      secondaryAction={
        onDeleteUser && (
          <IconButton edge="end" onClick={() => onDeleteUser(user)}>
            <DeleteIcon />
          </IconButton>
        )
      }
    >
      <ListItemContainer>
        <ListItemAvatar>
          <Avatar {...avatarProps} />
        </ListItemAvatar>
        <ListItemText {...textProps} />
      </ListItemContainer>
    </ListItem>
  );
};

const UsersList = (props: IUsersListProps) => {
  const { users, onDeleteUser, onSelectUser } = props;

  return (
    <List>
      {users.map((user, index) => {
        return (
          <UserListItem
            key={user.user_id}
            user={user}
            onSelectUser={onSelectUser}
            onDeleteUser={onDeleteUser}
            isLastItem={index === users.length - 1}
          />
        );
      })}
    </List>
  );
};

export default UsersList;
