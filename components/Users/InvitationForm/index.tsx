import React from 'react';
import generator from 'generate-password';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import Typography from '@mui/material/Typography';
import { useAuthContext } from '../../../context/authContext';
import { isOrgAdmin, isOrgUser, ORG_ADMIN_PREFIX, ORG_USER_PREFIX } from '../../../models/user';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';

interface IUserInvitationFormProps {
  isLoading: boolean;
  onSubmit: (data: any) => void;
  showOrgSwitcher?: boolean;
}

const schema = yup
  .object({
    name: yup.string().required(),
    email: yup.string().email().required(),
    password: yup.string().required(),
  })
  .required();

const UserInvitationForm = (props: IUserInvitationFormProps) => {
  const { isLoading, showOrgSwitcher, onSubmit } = props;
  const { organization } = useAuthContext();
  const {
    control,
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    setFocus,
  } = useForm({
    defaultValues: { email: '', name: '', password: '', isAdmin: false },
    mode: 'onTouched',
    resolver: yupResolver(schema),
    shouldUnregister: true,
  });

  const getRoleDisplayName = (role: string): string => {
    let displayRole = '';
    if (isOrgAdmin(role)) displayRole = `Admin ${role.substring(ORG_ADMIN_PREFIX.length)}`;
    if (isOrgUser(role)) displayRole = `Utilizator ${role.substring(ORG_USER_PREFIX.length)}`;
    return displayRole;
  };

  const generatePassword = () => {
    const password = generator.generate({
      length: 16,
      numbers: true,
    });

    setValue('password', password, { shouldTouch: false, shouldDirty: true });
    setFocus('password');
  };

  return (
    <Grid
      component="form"
      onSubmit={handleSubmit(onSubmit)}
      container
      spacing={2}
      justifyContent="flex-end"
    >
      <Grid item xs={12}>
        <Typography variant="body2" color="textSecondary">
          Adaugă utilizator
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <TextField
          fullWidth
          error={!!errors.name}
          label="Nume complet"
          required={true}
          disabled={isLoading}
          defaultValue=""
          size="small"
          {...register('name')}
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          fullWidth
          error={!!errors.email}
          label="Email"
          required={true}
          disabled={isLoading}
          defaultValue=""
          size="small"
          {...register('email')}
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          fullWidth
          error={!!errors.password}
          label="Parolă"
          required={true}
          disabled={isLoading}
          defaultValue=""
          size="small"
          {...register('password')}
        />
      </Grid>
      {showOrgSwitcher && organization && (
        <Grid item xs={6}>
          <Controller
            name="isAdmin"
            control={control}
            render={({ field }) => {
              const { value, onChange } = field;

              const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
                onChange(event.target.checked);
              };

              return (
                <FormControlLabel
                  control={<Checkbox checked={!!value} onChange={handleChange} />}
                  label={getRoleDisplayName(organization)}
                />
              );
            }}
          />
        </Grid>
      )}
      <Grid item xs={12} md={6}>
        <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
          <Box sx={{ mr: 1 }}>
            <Button
              variant="contained"
              color="secondary"
              type="button"
              onClick={generatePassword}
              fullWidth
            >
              Generează parolă
            </Button>
          </Box>
          <Box>
            <Button
              disabled={!!errors.email || isLoading}
              variant="contained"
              type="submit"
              fullWidth
            >
              Adaugă
            </Button>
          </Box>
        </Box>
      </Grid>
    </Grid>
  );
};

export default UserInvitationForm;
