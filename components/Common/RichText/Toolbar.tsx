import React from 'react';
import BoldIcon from '@mui/icons-material/FormatBold';
import ItalicIcon from '@mui/icons-material/FormatItalic';
import UnderlinedIcon from '@mui/icons-material/FormatUnderlined';
import IconButton from '@mui/material/IconButton';

const RichTextToolbar = () => {
  return (
    <div id="rte-toolbar">
      <select
        className="ql-header"
        defaultValue={''}
        // @ts-ignore
        onChange={(e) => e?.persist && e.persist()}
      >
        <option value="1">Title</option>
        <option value="2">Subtitle</option>
        <option value="">Normal</option>
      </select>

      <IconButton className="ql-bold">
        <BoldIcon />
      </IconButton>
      <IconButton className="ql-italic">
        <ItalicIcon />
      </IconButton>
      <IconButton className="ql-underline">
        <UnderlinedIcon />
      </IconButton>
    </div>
  );
};

export default RichTextToolbar;
