import React from 'react';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import Tooltip from '@mui/material/Tooltip';
import Typography from '@mui/material/Typography';

interface IPercentBarProps {
  color: string | undefined;
  label: string | React.ReactNode | undefined;
  value: number;
  displayValue?: string | undefined;
}

const PercentBarChart = (props: IPercentBarProps) => {
  const { label, displayValue, value, color } = props;

  const renderLabel = () => {
    if (label && typeof label === 'string') {
      return (
        <Tooltip title={label}>
          <Typography gutterBottom sx={{ mr: 3 }} noWrap>
            {label}
          </Typography>
        </Tooltip>
      );
    } else {
      return label;
    }
  };

  return (
    <>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'flex-end',
          justifyContent: 'space-between',
        }}
      >
        {renderLabel()}
        &nbsp;
        <Typography gutterBottom color="textSecondary">
          {displayValue || `${value.toFixed(0)}%`}
        </Typography>
      </Box>

      <LinearProgress
        variant="determinate"
        color="inherit"
        value={value}
        sx={{
          height: 14,
          borderRadius: 3,
          '& .MuiLinearProgress-bar': {
            backgroundColor: color,
            borderRadius: 3,
          },
          '&::before': {
            bgcolor: 'text.secondary',
          },
        }}
      />
    </>
  );
};

export default PercentBarChart;
