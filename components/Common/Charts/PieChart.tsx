import React from 'react';
import Typography from '@mui/material/Typography';
import orderBy from 'lodash/orderBy';
import { Cell, Pie, PieChart as RePieChart, ResponsiveContainer } from 'recharts';
import Box from '@mui/material/Box';

interface IPieChartData {
  name: string;
  value: number;
}

interface IPieChartProps {
  data: IPieChartData[];
  colors: string[];
}

const PieChart = (props: IPieChartProps) => {
  const { colors, data } = props;
  const chartData = orderBy(data, (info) => info.value, ['desc']);

  const maxValue = chartData[0].value;
  const startAngle = maxValue > 0.5 ? 270 + 180 * (1 - maxValue) : 90 - 180 * maxValue;

  return (
    <>
      <Box sx={{ width: '100%', height: 196, position: 'relative' }}>
        <ResponsiveContainer>
          <RePieChart>
            <Pie
              data={chartData}
              innerRadius={70}
              outerRadius={90}
              startAngle={startAngle}
              endAngle={360 + startAngle}
              paddingAngle={0}
              dataKey="value"
            >
              {chartData.map((entry, index) => (
                <Cell
                  key={`cell-${index}`}
                  fill={colors[index % colors.length]}
                  strokeWidth={index ? 8 : 0}
                />
              ))}
            </Pie>
          </RePieChart>
        </ResponsiveContainer>

        <div
          style={{
            position: 'absolute',
            left: 0,
            top: 0,
            height: '100%',
            width: '100%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            zIndex: 1,
          }}
        >
          <Typography variant="h5">
            <strong>{(100 * chartData[0].value).toFixed(0)}%</strong>
          </Typography>
        </div>
      </Box>
    </>
  );
};

export default PieChart;
