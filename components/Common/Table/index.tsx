import React, { useCallback, useState } from 'react';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { TableCellProps } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import { Breakpoint } from '@mui/system';
import { SxProps } from '@mui/material';
import _orderBy from 'lodash/orderBy';
import moment from 'moment/moment';
import TableTypography from './TableTypography';

interface IOrderByFieldCriteriaArgument {
  data: any;
}

interface IOrderByFieldCriteriaFunctions {
  [key: string]: (item: IOrderByFieldCriteriaArgument) => any;
}

interface IEnhancedTableHeadProps {
  headerRows: IHeaderRow[];
  order?: string;
  orderBy?: string;
  onRequestSort?: (field: string) => void;
}

interface IHeaderRow {
  id: string;
  label: string | React.ReactNode;
  align?: string;
  colSpan?: number;
  disableSorting?: boolean;
  hideOnSmallDevice?: boolean;
}

interface IEnhancedTableProps {
  smallUntilBreakpoint: Breakpoint;
  rows: IOrderByFieldCriteriaArgument[];
  headerRows: IHeaderRow[];
  initialOrder?: string;
  initialOrderByField?: string;
  PaperComponent?: React.FunctionComponent;
  paperComponentStyles?: SxProps;
  orderByFieldCriteriaFunctions?: IOrderByFieldCriteriaFunctions;
}

const renderData = (info: any, sx?: SxProps) =>
  typeof info === 'string' || typeof info === 'number' ? (
    <TableTypography label={info} sx={sx} />
  ) : (
    info
  );

const EnhancedTableHead = (props: IEnhancedTableHeadProps) => {
  const { headerRows, order, orderBy, onRequestSort } = props;

  const createSortHandler = (property: string) => () => {
    onRequestSort && onRequestSort(property);
  };

  return (
    <TableHead>
      <TableRow>
        {headerRows.map((row) => {
          const cellProps: TableCellProps = {
            // @ts-ignore
            align: row.align || 'center',
            sx: {
              bgcolor: 'transparent',
              typography: 'caption',
              fontWeight: 'light',
              color: 'textSecondary',
            },
            colSpan: 1,
          };

          if (row.colSpan) cellProps.colSpan = row.colSpan;

          if (!row.disableSorting) {
            // @ts-ignore
            cellProps.sortDirection = orderBy === row.id ? order : false;
            cellProps.children = (
              <TableSortLabel
                active={orderBy === row.id}
                // @ts-ignore
                direction={order}
                onClick={createSortHandler(row.id)}
              >
                {renderData(row.label)}
              </TableSortLabel>
            );
          } else {
            cellProps.children = renderData(row.label);
          }

          return <TableCell key={row.id} {...cellProps} />;
        })}
      </TableRow>
    </TableHead>
  );
};

const EnhancedTable = (props: IEnhancedTableProps) => {
  const {
    smallUntilBreakpoint,
    rows,
    headerRows,
    initialOrder,
    initialOrderByField,
    PaperComponent = Box,
    paperComponentStyles,
    orderByFieldCriteriaFunctions,
  } = props;

  const [order, setOrder] = useState(initialOrder || 'asc');
  const [orderBy, setOrderBy] = useState(initialOrderByField || 'createdAt');

  const theme = useTheme();
  const isSmallDevice = !useMediaQuery(theme.breakpoints.up(smallUntilBreakpoint), { noSsr: true });

  const handleRequestSort = (property: string) => {
    const isDesc = orderBy === property && order === 'desc';
    const newOrder = isDesc ? 'asc' : 'desc';

    setOrder(newOrder);
    setOrderBy(property);
  };

  const getRows = useCallback(() => {
    let iteratee: any = [`data.${orderBy}`];

    if (orderBy === 'createdAt') {
      iteratee = (row: IOrderByFieldCriteriaArgument) => moment(row.data?.createdAt).toDate();
    }
    // @ts-ignore
    else if (orderByFieldCriteriaFunctions?.[orderBy]) {
      // @ts-ignore
      iteratee = orderByFieldCriteriaFunctions[orderBy];
    }

    // @ts-ignore
    return _orderBy(rows, iteratee, [order]);
  }, [rows, order, orderBy, orderByFieldCriteriaFunctions]);

  return (
    <>
      {isSmallDevice ? (
        <>
          {getRows().map((row, index) => {
            return (
              <PaperComponent key={index} sx={{ my: 2, ...paperComponentStyles }}>
                {/* @ts-ignore */}
                {row.smallDeviceHeader ? <Box pt={1}>{row.smallDeviceHeader}</Box> : null}

                <Table>
                  <TableBody>
                    {headerRows
                      .filter((item) => !item.hideOnSmallDevice)
                      .map((headerItem) => {
                        // @ts-ignore
                        const displayInfo = row[headerItem.id];
                        const labelInfo = headerItem.label;

                        return (
                          <TableRow
                            key={headerItem.id}
                            hover
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                          >
                            <TableCell align="left">{renderData(labelInfo)}</TableCell>

                            <TableCell align="right">{renderData(displayInfo)}</TableCell>
                          </TableRow>
                        );
                      })}
                  </TableBody>
                </Table>
              </PaperComponent>
            );
          })}
        </>
      ) : (
        <PaperComponent sx={{ ...paperComponentStyles }}>
          <TableContainer>
            <Table stickyHeader sx={{ tableLayout: 'fixed' }}>
              <EnhancedTableHead
                headerRows={headerRows}
                order={order}
                orderBy={orderBy}
                onRequestSort={handleRequestSort}
              />
              <TableBody>
                {getRows().map((row, index) => {
                  return (
                    <TableRow
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      hover
                      role="checkbox"
                      tabIndex={-1}
                      // @ts-ignore
                      key={`${row?.name}${index}`}
                    >
                      {headerRows.map((headItem) => {
                        const { id, align, colSpan = 1 } = headItem;

                        // @ts-ignore
                        const tableCellProps: TableCellProps = {
                          // @ts-ignore
                          children: renderData(row[id]),
                          colSpan,
                          // @ts-ignore
                          align: align || 'center',
                        };

                        return <TableCell key={id} {...tableCellProps} />;
                      })}
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </PaperComponent>
      )}
    </>
  );
};

export default EnhancedTable;
