import React, { useCallback, useMemo } from 'react';
import Box from '@mui/material/Box';
import { IQuestion } from '../../../models/question';
import QuestionsResponseListItem from './Item';
import groupBy from 'lodash/groupBy';
import { ISurveyAnswer } from '../../../models/surveyAnswer';

interface IQuestionsResponseListProps {
  answeringQuestionId: string | undefined;
  questions: IQuestion[];
  surveyAnswers: ISurveyAnswer[];
  onAnswerChange: (answer: Partial<ISurveyAnswer>) => void;
}

const QuestionsResponseList = (props: IQuestionsResponseListProps) => {
  const { answeringQuestionId, questions, surveyAnswers, onAnswerChange } = props;

  const answersGroupedByQuestion = useMemo(() => {
    return surveyAnswers && groupBy(surveyAnswers, 'questionId');
  }, [surveyAnswers]);

  const getQuestionAnswer = useCallback(
    (questionId: string) => {
      return answersGroupedByQuestion?.[questionId]?.[0];
    },
    [answersGroupedByQuestion],
  );

  return (
    <>
      {questions.map((question) => {
        return (
          <Box key={question._id} sx={{ mb: 4 }}>
            <QuestionsResponseListItem
              answeringQuestionId={answeringQuestionId}
              question={question}
              onAnswerChange={onAnswerChange}
              userSurveyQuestionAnswer={getQuestionAnswer(question._id as string)}
            />
          </Box>
        );
      })}
    </>
  );
};

export default QuestionsResponseList;
