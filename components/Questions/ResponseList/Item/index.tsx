import React from 'react';
import CircleOutlinedIcon from '@mui/icons-material/CircleOutlined';
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import RadioButtonCheckedOutlinedIcon from '@mui/icons-material/RadioButtonCheckedOutlined';
import Box from '@mui/material/Box';
import ButtonBase from '@mui/material/ButtonBase';
import Card from '@mui/material/Card';
import LinearProgress from '@mui/material/LinearProgress';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import { IQuestion } from '../../../../models/question';
import {
  getQuestionDisplayAnswers,
  QUESTION_ANSWER_OTHER_TEXT,
  QUESTION_MULTIPLE_ANSWER_SEPARATOR,
} from '../../utils';
import { QUESTION_TYPE } from '../../../../services/questions';
import { ISurveyAnswer } from '../../../../models/surveyAnswer';
import { useDebouncedCallback } from 'use-debounce';

interface IQuestionsListItemProps {
  answeringQuestionId: string | undefined;
  question: IQuestion;
  userSurveyQuestionAnswer?: ISurveyAnswer;
  onAnswerChange: (answer: Partial<ISurveyAnswer>) => void;
}

const QuestionsListItem = (props: IQuestionsListItemProps) => {
  const { answeringQuestionId, question, userSurveyQuestionAnswer, onAnswerChange } = props;
  const userAnswer = userSurveyQuestionAnswer?.answer;

  const onAnswerInputChange = (answerText: string | undefined, isTextFieldAnswer?: boolean) => {
    const newAnswer: ISurveyAnswer = {
      answer: answerText === QUESTION_ANSWER_OTHER_TEXT ? ' ' : answerText,
      questionId: question._id,
      surveyId: question.surveyId,
      _id: userSurveyQuestionAnswer?._id,
    };

    const hasMultipleAnswers = question.type === QUESTION_TYPE.MULTIPLE_SELECT;

    if (hasMultipleAnswers) {
      // for multiple options we have
      // 1. User selected a new option => add answer _id to answer text
      // 2. User selected a checked option => remove answer _id from answer text
      // 3. User selected "other" => new answer added to answer text (' ')
      // 4. User added TEXT as "other" => TEXT gets replaced to answer text
      // 5. User selected "other" while other was selected => OTHER TEXT gets removed from answer text

      // @ts-ignore
      const answers = userAnswer?.split(QUESTION_MULTIPLE_ANSWER_SEPARATOR) || [];
      // @ts-ignore
      const answeredIndex = answers.indexOf(answerText);

      const hasAlreadyProvidedOtherAsAnswer = answers.findIndex((answer) => {
        // @ts-ignore
        const validQuestionIndex = question.answers.findIndex((a) => a._id === answer);

        return validQuestionIndex === -1;
      });

      if (answeredIndex >= 0) {
        // case #2 from comment above
        console.log('case #2');
        answers.splice(answeredIndex, 1);
      } else if (answerText === QUESTION_ANSWER_OTHER_TEXT) {
        // case #3 or case #5
        console.log('case #3 or case #5');

        if (hasAlreadyProvidedOtherAsAnswer === -1) {
          console.log('case #3');
          answers.push(' ');
        } else {
          console.log('case #5');
          answers.splice(hasAlreadyProvidedOtherAsAnswer, 1);
        }
      } else if (isTextFieldAnswer && hasAlreadyProvidedOtherAsAnswer !== -1) {
        // case #4
        console.log('case #4');
        // @ts-ignore
        answers[hasAlreadyProvidedOtherAsAnswer] = answerText;
      } else {
        // case #1
        console.log('case #1');
        // @ts-ignore
        answers.push(answerText);
      }

      newAnswer.answer = answers.join(QUESTION_MULTIPLE_ANSWER_SEPARATOR);
    }

    onAnswerChange(newAnswer);
  };

  const debouncedChange = useDebouncedCallback(onAnswerInputChange, 1000);

  const textFieldProps = {
    fullWidth: true,
    multiline: true,
    label: 'Răspuns',
    defaultValue: userAnswer || '',
    disabled: answeringQuestionId && answeringQuestionId !== question?._id,
    // @ts-ignore
    onChange: (ev) => debouncedChange(ev.target.value, true),
    ...(question.type === QUESTION_TYPE.LONG_ANSWER && { rows: 10 }),
  };

  const renderTextField = (defaultValue?: string) => {
    if (defaultValue) {
      textFieldProps.defaultValue = defaultValue;
    }

    return (
      <Box my={2}>
        {/* @ts-ignore */}
        <TextField {...textFieldProps} />
      </Box>
    );
  };

  const renderQuestionAnswers = (question: IQuestion) => {
    switch (question.type) {
      case QUESTION_TYPE.SHORT_ANSWER:
      case QUESTION_TYPE.LONG_ANSWER:
        return renderTextField();
      case QUESTION_TYPE.MULTIPLE_SELECT:
        const answers = userAnswer?.split(QUESTION_MULTIPLE_ANSWER_SEPARATOR) || [];
        let isOther: string | boolean = false;

        const answersObject = answers.reduce((acc, cur) => {
          // @ts-ignore
          const answerInfo = question?.answers.find((answer) => answer._id === cur);

          if (!answerInfo) {
            isOther = cur;
          }

          // @ts-ignore
          acc[cur] = answerInfo;

          return acc;
        }, {});

        return (
          <>
            {getQuestionDisplayAnswers(question).map((answer) => {
              const { _id, text } = answer;

              let icon = <CheckBoxOutlineBlankIcon sx={{ color: 'text.secondary' }} />;

              // @ts-ignore
              if (answersObject[_id] || (isOther && text === QUESTION_ANSWER_OTHER_TEXT)) {
                icon = <CheckBoxIcon sx={{ color: 'primary.main' }} />;
              }

              return (
                <Box mb={0.5} key={text}>
                  <ButtonBase
                    onClick={() => onAnswerInputChange(answer._id)}
                    sx={{
                      width: '100%',
                      textAlign: 'left',
                      justifyContent: 'flex-start',
                    }}
                  >
                    <Box my={1} sx={{ display: 'flex', alignItems: 'flex-start' }}>
                      <Box mr={1}>{icon}</Box>

                      <Box mt={0.5} sx={{ flexGrow: 1 }}>
                        <Typography>{text}</Typography>
                      </Box>
                    </Box>
                  </ButtonBase>
                </Box>
              );
            })}

            {/* @ts-ignore */}
            {isOther && renderTextField(isOther)}
          </>
        );
      default:
        const isOtherAnswer =
          userAnswer &&
          // @ts-ignore
          !question?.answers.find((answer) => answer._id === userAnswer);

        return (
          <>
            {getQuestionDisplayAnswers(question).map((answer) => {
              const { _id, text } = answer;

              let icon = <CircleOutlinedIcon sx={{ color: 'text.secondary' }} />;

              if (_id === userAnswer || (isOtherAnswer && text === QUESTION_ANSWER_OTHER_TEXT)) {
                icon = <RadioButtonCheckedOutlinedIcon sx={{ color: 'primary.main' }} />;
              }

              return (
                <Box mb={0.5} key={text}>
                  <ButtonBase
                    onClick={() => onAnswerInputChange(answer._id)}
                    sx={{
                      width: '100%',
                      textAlign: 'left',
                      justifyContent: 'flex-start',
                    }}
                  >
                    <Box my={1} sx={{ display: 'flex', alignItems: 'flex-start' }}>
                      <Box mr={1}>{icon}</Box>

                      <Box mt={0.5} sx={{ flexGrow: 1 }}>
                        <Typography>{text}</Typography>
                      </Box>
                    </Box>
                  </ButtonBase>
                </Box>
              );
            })}

            {isOtherAnswer && renderTextField()}
          </>
        );
    }
  };

  return (
    <>
      {answeringQuestionId === question._id && <LinearProgress />}

      <Card>
        <Box p={3}>
          <Typography variant="h6" sx={{ mb: 2 }}>
            {question.text}
          </Typography>

          {renderQuestionAnswers(question)}
        </Box>
      </Card>
    </>
  );
};

export default QuestionsListItem;
