import { IQuestion, IQuestionAnswer } from '../../models/question';

export const QUESTION_ANSWER_OTHER_TEXT = 'Altul';
export const QUESTION_MULTIPLE_ANSWER_SEPARATOR = '__';

export const getQuestionDisplayAnswers = (question: IQuestion): IQuestionAnswer[] => {
  // @ts-ignore
  const displayAnswers = question?.answers ? [...question.answers] : [];

  if (question.includeOtherAsOption) {
    displayAnswers.push({ _id: QUESTION_ANSWER_OTHER_TEXT, text: QUESTION_ANSWER_OTHER_TEXT });
  }

  return displayAnswers;
};
