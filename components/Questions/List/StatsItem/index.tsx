import React, { useState, useMemo, useCallback } from 'react';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import CloseIcon from '@mui/icons-material/Close';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import { IQuestion } from '../../../../models/question';
import {
  getQuestionDisplayAnswers,
  QUESTION_ANSWER_OTHER_TEXT,
  QUESTION_MULTIPLE_ANSWER_SEPARATOR,
} from '../../utils';
import { QUESTION_TYPE } from '../../../../services/questions';
import { ISurveyAnswer } from '../../../../models/surveyAnswer';
import PercentBarChart from '../../../Common/Charts/PercentBar';
import orderBy from 'lodash/orderBy';
import Grid from '@mui/material/Grid';
import Collapse from '@mui/material/Collapse';
import { COLORS } from '../../../../lib/theme';
import SurveyQuestionAnswerUsers from '../../../SurveyQuestionAnswer/AnswerUsers';
import Button from '@mui/material/Button';
import SurveyTextAnswers from '../../../SurveyQuestionAnswer/TextAnswers';
import PieChart from '../../../Common/Charts/PieChart';
import maxBy from 'lodash/maxBy';
import CreateCourseWithTitleButton from '../../../Courses/CreateCourseWithTitleButton';

interface IQuestionsListStatsItemProps {
  surveyQuestionAnswers: ISurveyAnswer[];
  onSelectAnswerUser: (answer: ISurveyAnswer) => void;
  question: IQuestion;
  disableCourseCreation: boolean;
}

const QuestionsListStatsItem = (props: IQuestionsListStatsItemProps) => {
  const { question, surveyQuestionAnswers, onSelectAnswerUser, disableCourseCreation } = props;
  const [showOthers, setShowOthers] = useState(false);
  const displayAnswers = useMemo(() => getQuestionDisplayAnswers(question), [question]);

  const answersGroupedByAnswer = useMemo(() => {
    const questionAnswersGroup = {};

    question?.answers?.forEach(
      // @ts-ignore
      (answer) => (questionAnswersGroup[answer._id] = []),
    );

    surveyQuestionAnswers?.forEach((questionAnswer) => {
      const { answer } = questionAnswer;

      let answers = [answer];

      if (answer?.includes(QUESTION_MULTIPLE_ANSWER_SEPARATOR)) {
        answers = answer.split(QUESTION_MULTIPLE_ANSWER_SEPARATOR);
      }

      for (const answer of answers) {
        const groupKey =
          // @ts-ignore
          questionAnswersGroup[answer] || !question.includeOtherAsOption
            ? answer?.toLowerCase()
            : QUESTION_ANSWER_OTHER_TEXT;

        // @ts-ignore
        if (!questionAnswersGroup[groupKey]) questionAnswersGroup[groupKey] = [];

        // @ts-ignore
        questionAnswersGroup[groupKey] &&
          // @ts-ignore
          questionAnswersGroup[groupKey].push({
            ...questionAnswer,
            answer,
          });
      }
    });

    return questionAnswersGroup;
  }, [surveyQuestionAnswers, question]);

  const sortedAnswers = useMemo(() => {
    return orderBy(
      Object.keys(answersGroupedByAnswer),
      // @ts-ignore
      [(key) => answersGroupedByAnswer[key].length],
      ['desc'],
    );
  }, [answersGroupedByAnswer]);
  // const sortedAnswers = Object.keys(answersGroupedByAnswer);

  const getAnswerPercentageValue = useCallback(
    (answerId: string) => {
      // @ts-ignore
      const distinctAnswers = answersGroupedByAnswer[answerId];

      return distinctAnswers?.length / surveyQuestionAnswers?.length || 0;
    },
    [answersGroupedByAnswer, surveyQuestionAnswers],
  );

  const onSelectUserFromAnswer = (answer: ISurveyAnswer) => {
    onSelectAnswerUser(answer);
  };

  const renderQuestionAnswers = () => {
    switch (question.type) {
      case QUESTION_TYPE.SHORT_ANSWER:
      case QUESTION_TYPE.LONG_ANSWER:
        return (
          <SurveyTextAnswers
            answers={surveyQuestionAnswers}
            onSelectUserFromAnswer={onSelectUserFromAnswer}
          />
        );
      default:
        return (
          <>
            {surveyQuestionAnswers?.length ? (
              <Grid container spacing={2}>
                <Grid item xs={12} md={7}>
                  {sortedAnswers?.map((answerId, index) => {
                    // @ts-ignore
                    const distinctAnswers = answersGroupedByAnswer[answerId];
                    const answerInfo = displayAnswers.find((answer) => answer._id === answerId);
                    const answerText = answerInfo?.text || answerId;
                    const value = getAnswerPercentageValue(answerId);
                    const isOther = answerText === QUESTION_ANSWER_OTHER_TEXT;
                    const color = COLORS[index % COLORS.length];

                    return (
                      <Box key={answerId} my={3}>
                        <PercentBarChart
                          label={
                            isOther ? (
                              <Box>
                                <Typography gutterBottom sx={{ mr: 3 }}>
                                  {answerText}&nbsp;
                                  <Button
                                    onClick={() => setShowOthers(!showOthers)}
                                    component="span"
                                    size="small"
                                    color="inherit"
                                    sx={{
                                      ml: 0.5,
                                      px: 1,
                                      justifyContent: 'space-between',
                                    }}
                                    endIcon={
                                      showOthers ? (
                                        <CloseIcon sx={{ mt: -0.5 }} />
                                      ) : (
                                        <KeyboardArrowDownIcon sx={{ mt: -0.5 }} />
                                      )
                                    }
                                  >
                                    ( {showOthers ? 'Ascunde' : 'Afişează'} raspunsuri )
                                  </Button>
                                </Typography>

                                <Collapse in={showOthers}>
                                  <Box
                                    sx={{
                                      pl: 3,
                                      mb: 5,
                                      borderLeft: 1,
                                      borderLeftColor: 'divider',
                                    }}
                                  >
                                    <SurveyTextAnswers
                                      answers={distinctAnswers}
                                      onSelectUserFromAnswer={onSelectUserFromAnswer}
                                    />
                                  </Box>
                                </Collapse>
                              </Box>
                            ) : (
                              answerText
                            )
                          }
                          displayValue={`${distinctAnswers.length}`}
                          value={value * 100}
                          color={color}
                        />

                        <Box sx={{ display: 'flex' }}>
                          <Box sx={{ flexGrow: 1 }}>
                            <SurveyQuestionAnswerUsers
                              answers={distinctAnswers}
                              onClick={onSelectUserFromAnswer}
                            />
                          </Box>
                          {!disableCourseCreation && (
                            <Box ml={2} mt={1.5}>
                              <CreateCourseWithTitleButton title={`${answerText}`} />
                            </Box>
                          )}
                        </Box>
                      </Box>
                    );
                  })}
                </Grid>

                <Grid item xs={12} md={5}>
                  {(() => {
                    const chartAnswerData = sortedAnswers?.map((answerId) => {
                      const answerInfo = displayAnswers.find((answer) => answer._id === answerId);
                      const answerText = answerInfo?.text || answerId;
                      const value = getAnswerPercentageValue(answerId);

                      return {
                        name: answerText,
                        value,
                      };
                    });

                    const topAnswer = maxBy(chartAnswerData, 'value');

                    return (
                      <>
                        <PieChart data={chartAnswerData} colors={COLORS} />

                        <Box sx={{ mx: 5, my: 2 }}>
                          <Typography variant="body1" align="center" color="textSecondary">
                            Cel mai frecvent răspuns:
                          </Typography>

                          <Box sx={{ my: 1 }}>
                            <Typography variant="body1" align="center" textOverflow="ellipsis">
                              {topAnswer?.name}
                            </Typography>
                          </Box>
                        </Box>
                      </>
                    );
                  })()}
                </Grid>
              </Grid>
            ) : null}
          </>
        );
    }
  };

  return (
    <Card>
      <Box p={3}>
        <Typography variant="h6" sx={{ flexGrow: 1 }}>
          {question.text}
        </Typography>

        <Box mb={2}>
          <Typography color="textSecondary" variant="body2">
            {surveyQuestionAnswers?.length
              ? `${surveyQuestionAnswers?.length} răspunsuri`
              : 'Nu există  încă niciun răspuns pentru această  întrebare'}
          </Typography>
        </Box>

        {renderQuestionAnswers()}
      </Box>
    </Card>
  );
};

export default QuestionsListStatsItem;
