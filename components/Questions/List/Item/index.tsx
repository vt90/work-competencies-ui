import React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CircleOutlinedIcon from '@mui/icons-material/CircleOutlined';
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import RadioButtonCheckedOutlinedIcon from '@mui/icons-material/RadioButtonCheckedOutlined';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import { IQuestion } from '../../../../models/question';
import {
  getQuestionDisplayAnswers,
  QUESTION_ANSWER_OTHER_TEXT,
  QUESTION_MULTIPLE_ANSWER_SEPARATOR,
} from '../../utils';
import { QUESTION_TYPE } from '../../../../services/questions';
import { ISurveyAnswer } from '../../../../models/surveyAnswer';

interface IQuestionsListItemProps {
  question: IQuestion;
  onEditQuestion: (org: IQuestion) => void;
  onDeleteQuestion: (org: IQuestion) => void;
  readonly: boolean;
  showAnswer: boolean;
  individual?: string;
  userSurveyQuestionAnswers?: ISurveyAnswer;
}

const QuestionsListItem = (props: IQuestionsListItemProps) => {
  const {
    question,
    onEditQuestion,
    onDeleteQuestion,
    readonly,
    showAnswer,
    userSurveyQuestionAnswers,
  } = props;
  const userAnswer = showAnswer && userSurveyQuestionAnswers?.answer;

  const textFieldProps = {
    disabled: true,
    fullWidth: true,
    multiline: true,
    label: 'Răspuns',
    ...(question.type === QUESTION_TYPE.LONG_ANSWER && { rows: 10 }),
  };

  const renderUserAnswerText = (defaultText?: string) => {
    return (
      <Typography color="textSecondary">
        {defaultText || userAnswer || 'Nu există încă niciun răspuns pentru această întrebare'}
      </Typography>
    );
  };

  const renderQuestionAnswers = (question: IQuestion) => {
    switch (question.type) {
      case QUESTION_TYPE.SHORT_ANSWER:
      case QUESTION_TYPE.LONG_ANSWER:
        return (
          <Box mb={2}>
            {showAnswer ? renderUserAnswerText() : <TextField {...textFieldProps} />}
          </Box>
        );
      case QUESTION_TYPE.MULTIPLE_SELECT:
        const answers =
          // @ts-ignore
          (userAnswer?.split && userAnswer?.split(QUESTION_MULTIPLE_ANSWER_SEPARATOR)) || [];
        let isOther: string | boolean = false;

        // @ts-ignore
        const answersObject = answers.reduce((acc, cur) => {
          // @ts-ignore
          const answerInfo = question?.answers.find((answer) => answer._id === cur);

          if (!answerInfo) {
            isOther = cur;
          }

          // @ts-ignore
          acc[cur] = answerInfo;

          return acc;
        }, {});

        const noAnswer = showAnswer && !userAnswer;

        return (
          <>
            {getQuestionDisplayAnswers(question).map((answer) => {
              const { _id, text } = answer;

              let icon = <CheckBoxOutlineBlankIcon sx={{ color: 'text.secondary' }} />;

              // @ts-ignore
              if (answersObject[_id] || (isOther && text === QUESTION_ANSWER_OTHER_TEXT)) {
                icon = <CheckBoxIcon sx={{ color: 'text.primary' }} />;
              }

              return (
                <Box key={text} my={1} sx={{ display: 'flex', alignItems: 'flex-start' }}>
                  <Box mr={1}>{icon}</Box>

                  <Box mt={0.5} sx={{ flexGrow: 1 }}>
                    <Typography>{text}</Typography>
                  </Box>
                </Box>
              );
            })}

            {/* @ts-ignore */}
            {(isOther || noAnswer) && <Box my={2}>{renderUserAnswerText(isOther)}</Box>}
          </>
        );
      default:
        const isOtherAnswer =
          userAnswer &&
          // @ts-ignore
          !question?.answers.find((answer) => answer._id === userAnswer);
        const noAnswerProvided = showAnswer && !userAnswer;
        return (
          <>
            {getQuestionDisplayAnswers(question).map((answer) => {
              const { _id, text, isCorrectAnswer } = answer;

              let icon = <CircleOutlinedIcon sx={{ color: 'text.secondary' }} />;

              if (_id === userAnswer || (isOtherAnswer && text === QUESTION_ANSWER_OTHER_TEXT)) {
                icon = <RadioButtonCheckedOutlinedIcon sx={{ color: 'text.primary' }} />;
              }

              if (isCorrectAnswer) {
                // @ts-ignore
                icon = <CheckCircleIcon color={_id === userAnswer ? 'text.primary' : 'primary'} />;
              }

              return (
                <Box key={text} my={1} sx={{ display: 'flex', alignItems: 'flex-start' }}>
                  <Box mr={1}>{icon}</Box>

                  <Box mt={0.5} sx={{ flexGrow: 1 }}>
                    <Typography>{text}</Typography>
                  </Box>
                </Box>
              );
            })}

            {(isOtherAnswer || noAnswerProvided) && <Box my={2}>{renderUserAnswerText()}</Box>}
          </>
        );
    }
  };

  return (
    <Card>
      <Box p={3}>
        <Box mb={2} sx={{ display: 'flex', alignItems: 'flex-start' }}>
          <Typography sx={{ flexGrow: 1 }}>{question.text}</Typography>

          {!readonly && (
            <>
              <IconButton sx={{ mt: -1 }} onClick={() => onEditQuestion(question)}>
                <EditIcon />
              </IconButton>

              <IconButton sx={{ mt: -1 }} onClick={() => onDeleteQuestion(question)}>
                <DeleteIcon />
              </IconButton>
            </>
          )}
        </Box>

        {renderQuestionAnswers(question)}
      </Box>
    </Card>
  );
};

export default QuestionsListItem;
