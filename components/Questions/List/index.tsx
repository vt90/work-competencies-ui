import React, { useCallback, useMemo } from 'react';
import Box from '@mui/material/Box';
import { IQuestion } from '../../../models/question';
import QuestionsListItem from './Item';
import { ISurvey, SURVEY_VIEWS } from '../../../models/survey';
import QuestionsListStatsItem from './StatsItem';
import groupBy from 'lodash/groupBy';
import { ISurveyAnswer } from '../../../models/surveyAnswer';

interface IQuestionsListProps {
  survey: ISurvey;
  questions: IQuestion[];
  onEditQuestion: (org: IQuestion) => void;
  onDeleteQuestion: (org: IQuestion) => void;
  onSelectAnswerUser: (answer: ISurveyAnswer) => void;
  readonly: boolean;
  view: string;
  individual: string | undefined;
  disableCourseCreation?: boolean;
}

const QuestionsList = (props: IQuestionsListProps) => {
  const {
    survey,
    questions,
    onEditQuestion,
    onDeleteQuestion,
    onSelectAnswerUser,
    view,
    individual,
    disableCourseCreation,
  } = props;
  const surveyQuestionAnswers = useMemo(
    () => survey && groupBy(survey.surveyAnswers, 'questionId'),
    [survey],
  );

  const getUserSurveyQuestionAnswer = useCallback(
    (questionId: string) =>
      surveyQuestionAnswers?.[questionId]?.find(
        (answer: ISurveyAnswer) => answer.userId === individual,
      ),
    [individual, surveyQuestionAnswers],
  );

  return (
    <>
      {questions.map((question) => {
        return (
          <Box key={question._id} sx={{ mb: 4 }}>
            {(view === SURVEY_VIEWS.STRUCT ||
              (view === SURVEY_VIEWS.INDIVIDUALS && individual)) && (
              <QuestionsListItem
                question={question}
                onEditQuestion={onEditQuestion}
                onDeleteQuestion={onDeleteQuestion}
                readonly={view === SURVEY_VIEWS.INDIVIDUALS}
                individual={individual}
                userSurveyQuestionAnswers={getUserSurveyQuestionAnswer(`${question._id}`)}
                showAnswer={!!(view === SURVEY_VIEWS.INDIVIDUALS && individual)}
              />
            )}
            {view === SURVEY_VIEWS.RESULTS && (
              <QuestionsListStatsItem
                question={question}
                onSelectAnswerUser={onSelectAnswerUser}
                // @ts-ignore
                surveyQuestionAnswers={surveyQuestionAnswers[question?._id]}
                disableCourseCreation={!!disableCourseCreation}
              />
            )}
          </Box>
        );
      })}
    </>
  );
};

export default QuestionsList;
