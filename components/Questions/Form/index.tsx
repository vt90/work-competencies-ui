import React from 'react';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Grid from '@mui/material/Grid';
import DoneAllIcon from '@mui/icons-material/DoneAll';
import RadioButtonCheckedOutlinedIcon from '@mui/icons-material/RadioButtonCheckedOutlined';
import ShortTextOutlinedIcon from '@mui/icons-material/ShortTextOutlined';
import SubjectOutlinedIcon from '@mui/icons-material/SubjectOutlined';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { IQuestion } from '../../../models/question';
import QuestionAnswersFieldArray from './questionAnswersFieldArray';
import { QUESTION_TYPE } from '../../../services/questions';
import Typography from '@mui/material/Typography';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import { QUESTION_ANSWER_OTHER_TEXT } from '../utils';

interface IQuestionsFormProps {
  isLoading: boolean;
  initialValues: IQuestion;
  isQuiz: boolean;
  open: boolean;
  onClose: () => void;
  onSubmit: (data: any) => void;
}

const schema = yup
  .object({
    text: yup.string().required(),
    type: yup.number().required(),
  })
  .required();

const QUESTION_TYPES = [
  {
    name: 'Grilă',
    value: QUESTION_TYPE.RADIO_SELECT,
    icon: RadioButtonCheckedOutlinedIcon,
  },
  {
    name: 'Răspuns multiplu',
    value: QUESTION_TYPE.MULTIPLE_SELECT,
    icon: DoneAllIcon,
  },
  {
    name: 'Răspuns scurt',
    value: QUESTION_TYPE.SHORT_ANSWER,
    icon: ShortTextOutlinedIcon,
  },
  {
    name: 'Răspuns deschis',
    value: QUESTION_TYPE.LONG_ANSWER,
    icon: SubjectOutlinedIcon,
  },
];

const QuestionsForm = (props: IQuestionsFormProps) => {
  const { isLoading, isQuiz, initialValues, open, onClose, onSubmit } = props;
  const {
    control,
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    getValues,
    watch,
  } = useForm({
    defaultValues: initialValues,
    mode: 'onTouched',
    resolver: yupResolver(schema),
    shouldUnregister: true,
  });

  const isUpdate = !!initialValues._id;
  const questionType = watch('type');
  const isChoiceAnswer =
    questionType == QUESTION_TYPE.RADIO_SELECT ||
    questionType == QUESTION_TYPE.MULTIPLE_SELECT ||
    (questionType === undefined && initialValues.type === QUESTION_TYPE.RADIO_SELECT) ||
    (questionType === undefined && initialValues.type === QUESTION_TYPE.MULTIPLE_SELECT);

  return (
    <Dialog
      open={open}
      fullWidth={true}
      maxWidth="md"
      keepMounted={false}
      onClose={onClose}
      // @ts-ignore
      PaperComponent="form"
      onSubmit={handleSubmit(onSubmit)}
    >
      <DialogTitle>{isUpdate ? 'Modifică întrebare' : 'Adaugă întrebare nouă'}</DialogTitle>
      <DialogContent dividers>
        <Box my={3}>
          <Grid container spacing={2}>
            <Grid item xs={12} md={9}>
              <Typography variant="subtitle2" color="textSecondary" gutterBottom>
                Întrebarea*
              </Typography>

              <TextField
                fullWidth
                error={!!errors.text}
                helperText={!!errors.text && 'Întrebarea este necesară'}
                required={true}
                multiline
                maxRows={10}
                sx={{
                  '& legend': { display: 'none' },
                  '& fieldset': { top: 0 },
                }}
                {...register('text')}
              />
            </Grid>
            <Grid item xs={12} md={3}>
              <Typography variant="subtitle2" color="textSecondary" gutterBottom>
                Tipul întrebării
              </Typography>
              <Controller
                name="type"
                control={control}
                defaultValue={initialValues.type}
                render={({ field }) => {
                  const { value, onChange, ...rest } = field;

                  return (
                    <Select
                      value={`${value}`}
                      onChange={onChange}
                      disabled={isQuiz}
                      {...rest}
                      fullWidth
                      sx={{
                        '& legend': { display: 'none' },
                        '& fieldset': { top: 0 },
                      }}
                    >
                      {QUESTION_TYPES.map((type) => (
                        <MenuItem key={type.value} value={`${type.value}`}>
                          <type.icon sx={{ mt: -0.5, mr: 1.5 }} fontSize="small" />
                          {type.name}
                        </MenuItem>
                      ))}
                    </Select>
                  );
                }}
              />
            </Grid>
          </Grid>
        </Box>
        <Box
          sx={{
            ...(!isChoiceAnswer && {
              display: 'none',
            }),
          }}
        >
          {isQuiz ? null : (
            <Box mt={-1}>
              <Controller
                name="includeOtherAsOption"
                control={control}
                render={({ field }) => {
                  const { value, onChange } = field;

                  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
                    onChange(event.target.checked);
                  };

                  return (
                    <FormControlLabel
                      control={<Checkbox checked={!!value} onChange={handleChange} />}
                      label={`Permite "${QUESTION_ANSWER_OTHER_TEXT}" ca opțiune de răspuns`}
                    />
                  );
                }}
              />
            </Box>
          )}

          <Box my={3}>
            <Grid container spacing={2}>
              <Grid item xs={12} md={9}>
                <Box mr={-6}>
                  <QuestionAnswersFieldArray
                    control={control}
                    register={register}
                    errors={errors}
                    isQuiz={isQuiz}
                    setValue={setValue}
                    getValues={getValues}
                  />
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </DialogContent>
      <DialogActions>
        <Button disabled={isLoading} onClick={onClose}>
          Anulare
        </Button>

        <Button disabled={isLoading} variant="contained" type="submit">
          {isUpdate ? 'Modifică' : 'Adaugă'}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default QuestionsForm;
