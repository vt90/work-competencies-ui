import React from 'react';
import Box from '@mui/material/Box';
import ButtonBase from '@mui/material/ButtonBase';
import IconButton from '@mui/material/IconButton';
import CircleOutlinedIcon from '@mui/icons-material/CircleOutlined';
import AddCircleOutlineOutlinedIcon from '@mui/icons-material/AddCircleOutlineOutlined';
import CloseOutlinedIcon from '@mui/icons-material/CloseOutlined';
import TextField from '@mui/material/TextField';
import { useFieldArray, Control, Controller } from 'react-hook-form';
// import * as yup from 'yup';
import { IQuestionAnswer } from '../../../models/question';
import YesNoToggle from '../../Common/YesNoToggle';

interface IQuestionAnswersFieldArrayProps {
  control: Control;
  register: any;
  setValue: any;
  getValues: any;
  errors: any;
  isQuiz: boolean;
}

// ToDo validate answers
// const schema = yup
//   .object({
//     name: yup.string().required(),
//     description: yup.string().required(),
//   })
//   .required();

const QuestionAnswersFieldArray = (props: IQuestionAnswersFieldArrayProps) => {
  const { isQuiz, control, register, errors, setValue, getValues } = props;

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'answers',
  });

  return (
    <Box mt={6}>
      {fields.map((item, index) => {
        return (
          <Box key={item.id || index}>
            <input type="hidden" {...register(`answers.${index}._id`)} />

            <Box
              sx={{
                display: 'flex',
                alignItems: 'center',
                my: 2,
              }}
            >
              <Box mr={1}>
                <CircleOutlinedIcon />
              </Box>
              <TextField
                fullWidth
                error={!!errors.text}
                label={`Răspuns #${index + 1}`}
                required={true}
                // helperText={!!errors.text && 'Întrebarea este necesară'}
                multiline
                rows={1}
                {...register(`answers.${index}.text`)}
              />
              <Box ml={1}>
                <IconButton disabled={fields.length === 1} onClick={() => remove(index)}>
                  <CloseOutlinedIcon />
                </IconButton>
              </Box>
            </Box>
            {isQuiz ? (
              <Box mr={6} mt={-1} mb={6} sx={{ display: 'flex', justifyContent: 'flex-end' }}>
                <Controller
                  name={`answers.${index}.isCorrectAnswer`}
                  control={control}
                  render={({ field }) => {
                    const { value, onChange } = field;

                    // const handleChange = (value: boolean) => {
                    //   const currentValues = getValues();
                    //   const otherCorrectAnswerSelected = currentValues.answers.findIndex(
                    //     (answer: IQuestionAnswer) => {
                    //       return !!answer?.isCorrectAnswer;
                    //     },
                    //   );
                    //
                    //   if (otherCorrectAnswerSelected > -1) {
                    //     setValue(`answers.${otherCorrectAnswerSelected}.isCorrectAnswer`, false);
                    //   }
                    //   onChange(value);
                    // };

                    return (
                      <YesNoToggle
                        // @ts-ignore
                        yesColor="primary"
                        // @ts-ignore
                        noColor="secondary"
                        value={!!value}
                        label={`Corect`}
                        onValueChange={onChange}
                      />
                    );
                  }}
                />
              </Box>
            ) : null}
          </Box>
        );
      })}
      <ButtonBase>
        <Box
          sx={{
            color: 'textPrimary',
            display: 'flex',
            alignItems: 'center',
            my: 2,
          }}
          onClick={() => append({})}
        >
          <Box mr={1}>
            <AddCircleOutlineOutlinedIcon />
          </Box>

          <strong>Adaugă opțiune</strong>
        </Box>
      </ButtonBase>
    </Box>
  );
};

export default QuestionAnswersFieldArray;
