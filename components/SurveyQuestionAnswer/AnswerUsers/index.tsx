import React from 'react';
import Box from '@mui/material/Box';
import ButtonBase from '@mui/material/ButtonBase';
import { ISurveyAnswer } from '../../../models/surveyAnswer';

interface ISurveyQuestionAnswerUsersProps {
  answers: ISurveyAnswer[];
  onClick: (answer: ISurveyAnswer) => void;
  selectedAnswer?: ISurveyAnswer;
}

const SurveyQuestionAnswerUsers = (props: ISurveyQuestionAnswerUsersProps) => {
  const { answers, onClick, selectedAnswer } = props;

  return (
    <Box sx={{ mt: 1, display: 'flex', flexWrap: 'wrap' }}>
      {answers.map((answer) => {
        const userDisplayName = answer.userName || answer.userEmail;
        const selected = selectedAnswer?.userId === answer?.userId;

        return (
          <ButtonBase
            sx={{
              mb: 1,
              mr: 1.5,
              px: 1.25,
              py: 0.5,
              position: 'relative',
              color: 'text.primary',
              ...(selected && {
                boxShadow: 6,
                bgcolor: 'text.secondary',
                color: '#FFFFFF',
              }),
              '&:before': {
                content: '""',
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: '100%',
                bgcolor: 'text.secondary',
                opacity: 0.2,
              },
            }}
            key={userDisplayName}
            onClick={() => onClick(answer)}
          >
            {userDisplayName} {answer?.displayResult || ''}
          </ButtonBase>
        );
      })}
    </Box>
  );
};

export default SurveyQuestionAnswerUsers;
