import React from 'react';
import Box from '@mui/material/Box';
import { ISurveyAnswer } from '../../../models/surveyAnswer';
import IconButton from '@mui/material/IconButton';
import Avatar from '@mui/material/Avatar';
import { COLORS } from '../../../lib/theme';
import SurveyQuestionAnswerUsers from '../AnswerUsers';
import Typography from '@mui/material/Typography';
import CreateCourseWithTitleButton from '../../Courses/CreateCourseWithTitleButton';

interface ISurveyTextAnswersProps {
  answers: ISurveyAnswer[];
  onSelectUserFromAnswer: (answer: ISurveyAnswer) => void;
}

const SurveyTextAnswers = (props: ISurveyTextAnswersProps) => {
  const { answers, onSelectUserFromAnswer } = props;

  return (
    <>
      {answers?.map((answer, index) => {
        return (
          <Box key={answer._id} sx={{ display: 'flex' }} my={2}>
            <Box mr={1} mt={0.5}>
              <IconButton
                onClick={() => onSelectUserFromAnswer(answer)}
                size="small"
                sx={{ padding: 0 }}
              >
                <Avatar
                  sx={{
                    background: COLORS[index % COLORS.length],
                  }}
                >
                  {answer.userName?.charAt(0).toUpperCase()}
                </Avatar>
              </IconButton>
            </Box>
            <Box sx={{ flexGrow: 1 }}>
              <SurveyQuestionAnswerUsers answers={[answer]} onClick={onSelectUserFromAnswer} />

              <Box sx={{ display: 'flex', alignItems: 'flex-end' }}>
                <Box sx={{ flexGrow: 1 }}>
                  <Typography>{answer.answer}</Typography>
                </Box>

                <Box ml={2} mb={1}>
                  <CreateCourseWithTitleButton title={`${answer.answer}`} />
                </Box>
              </Box>
            </Box>
          </Box>
        );
      })}
    </>
  );
};

export default SurveyTextAnswers;
