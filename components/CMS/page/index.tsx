import React, { useState } from 'react';
import Head from 'next/head';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { createOrUpdateCmsSection, deleteCmsSection, getCMSSections } from '../../../services/cms';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import LoadingButton from '@mui/lab/LoadingButton';
import Typography from '@mui/material/Typography';
import AdminPage from '../../../components/AdminPage';
import ListLoadingIndicator from '../../../components/Common/ListLoadingIndcator';
import CMSSectionCreateForm from '../CMSForm';
import { ICmsSection } from '../../../models/cmsSection';
import Dialog from '../../../components/Common/Dialog';
import NoDataAvailable from '../../../components/Common/NoDataAvailable';
import { useAuthContext } from '../../../context/authContext';
import { isSuperAdmin } from '../../../models/user';
import CmsSectionList from '../List';

const FETCH_QUERY_KEY = ['cms-sections'];

const CMSPage = () => {
  const { user, accessToken } = useAuthContext();

  const [cmsSection, setCMSSection] = useState<Partial<ICmsSection> | null>(null);

  const [deletingCMSSectionId, setShowDeletingCMSSectionId] = useState<ICmsSection | null>(null);

  const queryClient = useQueryClient();

  const { isLoading, data: cmsSections } = useQuery(FETCH_QUERY_KEY, () =>
    getCMSSections({
      orgId: process.env.NEXT_PUBLIC_TENANT,
    }),
  );

  const { isLoading: isDeleting, mutate: deleteCMSSectionById } = useMutation(deleteCmsSection, {
    onSuccess: async () => {
      await queryClient.invalidateQueries(FETCH_QUERY_KEY);
      setShowDeletingCMSSectionId(null);
    },
  });

  const { isLoading: isCreating, mutate: create } = useMutation(createOrUpdateCmsSection, {
    onSuccess: async () => {
      await queryClient.invalidateQueries(FETCH_QUERY_KEY);
      setCMSSection(null);
    },
  });

  const onCreate = (values: Partial<ICmsSection>) => {
    // @ts-ignore
    values.orgId = process.env.NEXT_PUBLIC_TENANT;
    // @ts-ignore
    values.accessToken = accessToken;
    create(values);
  };

  const canCreate =
    // @ts-ignore
    user && user.userRoles && user.userRoles.find((role) => isSuperAdmin(role.name));

  // @ts-ignore
  return (
    <div>
      <Head>
        <title>{process.env.NEXT_PUBLIC_TENANT_NAME}</title>
      </Head>

      <AdminPage
        actions={
          canCreate ? (
            <>
              <Button onClick={() => setCMSSection({})} variant="contained">
                Adaugă Sectiune
              </Button>
            </>
          ) : null
        }
        // title={process.env.NEXT_PUBLIC_TENANT_NAME}
        title=""
        pages={[{ url: '/', name: `${process.env.NEXT_PUBLIC_TENANT_NAME}` }]}
      >
        {cmsSections?.length || isLoading ? (
          <>
            {isLoading && <ListLoadingIndicator />}

            {cmsSections?.length && (
              <CmsSectionList
                canCreate={canCreate}
                cmsSections={cmsSections}
                // @ts-ignore
                {...(canCreate && {
                  onDeleteCmsSection: setShowDeletingCMSSectionId,
                  onSelectCmsSection: setCMSSection,
                })}
              />
            )}
          </>
        ) : (
          <NoDataAvailable content="În momentul de faţă nu există sectiuni disponibile pe platformă" />
        )}

        {cmsSection && (
          <CMSSectionCreateForm
            isLoading={isCreating}
            open={!!cmsSection}
            onClose={() => setCMSSection(null)}
            onSubmit={onCreate}
            initialValues={cmsSection}
            // @ts-ignore
            onDeleteCmsSectionFile={null}
          />
        )}

        <Dialog
          actions={
            <Box ml={1}>
              {deletingCMSSectionId && (
                <LoadingButton
                  variant="contained"
                  onClick={() =>
                    deletingCMSSectionId && deleteCMSSectionById(`${deletingCMSSectionId?._id}`)
                  }
                  loading={isDeleting}
                >
                  Șterge
                </LoadingButton>
              )}
            </Box>
          }
          content={
            <>
              <Typography color="textSecondary" textAlign="center">
                Sunteți siguri că doriți să ștergeți cursul
                <strong>
                  <i>&quot;{deletingCMSSectionId?.title}&quot;</i>
                </strong>
                ?
              </Typography>
            </>
          }
          title="Confirmati ștergerea?"
          onClose={() => {
            setShowDeletingCMSSectionId(null);
          }}
          open={!!deletingCMSSectionId}
        />
      </AdminPage>
    </div>
  );
};

export default CMSPage;
