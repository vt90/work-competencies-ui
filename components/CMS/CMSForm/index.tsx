import React from 'react';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import LinearProgress from '@mui/material/LinearProgress';
import { ICmsSection } from '../../../models/cmsSection';
import dynamic from 'next/dynamic';
import StickyNote2SharpIcon from '@mui/icons-material/StickyNote2Sharp';
import { ListItem, ListItemSecondaryAction } from '@mui/material';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import ListItemText from '@mui/material/ListItemText';
import moment from 'moment/moment';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import DeleteIcon from '@mui/icons-material/Delete';
import IconButton from '@mui/material/IconButton';
const RichText = dynamic(() => import('../../Common/RichText'), {
  ssr: false,
});

interface ICmsSectionFormProps {
  initialValues: Partial<ICmsSection>;
  isLoading: boolean;
  open: boolean;
  onClose: () => void;
  onSubmit: (data: any) => void;
  onDeleteCmsSectionFile: (index: number) => void;
}
const schema = yup
  .object({
    title: yup.string().required(),
    description: yup.string(),
  })
  .required();

const CmsSectionForm = (props: ICmsSectionFormProps) => {
  const { initialValues, isLoading, open, onClose, onSubmit, onDeleteCmsSectionFile } = props;
  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    control,
    watch,
  } = useForm({
    defaultValues: initialValues,
    mode: 'onTouched',
    resolver: yupResolver(schema),
    shouldUnregister: true,
  });

  const uploadedFiles = watch('uploadFiles');

  return (
    <Dialog
      open={open}
      keepMounted={false}
      onClose={onClose}
      // @ts-ignore
      PaperComponent="form"
      maxWidth="md"
      fullWidth
      onSubmit={handleSubmit(onSubmit)}
    >
      <DialogTitle>
        {initialValues?._id ? 'Modifică sectiune' : 'Creează sectiune nouă'}
      </DialogTitle>
      <DialogContent dividers>
        <input type="hidden" {...register(`_id`)} />
        <Box my={3}>
          <TextField
            fullWidth
            error={!!errors.title}
            label="Titlu"
            required={true}
            disabled={isLoading}
            helperText={!!errors.title && 'Titlul este necesar'}
            defaultValue=""
            {...register('title')}
          />
        </Box>

        <Box my={3}>
          <Controller
            render={({ field }) => {
              const { value, onChange } = field;

              return (
                <RichText
                  disableStickyToolbar={true}
                  defaultValue={value}
                  onChange={(value) => onChange(value)}
                  sx={{
                    // @ts-ignore
                    border: (theme) => `1px solid ${theme.palette.divider}!important`,
                    boxShadow: 0,
                    '& .ql-container': {
                      border: 'none !important',
                      minHeight: '300px',
                      p: 2,
                    },
                  }}
                />
              );
            }}
            name="description"
            control={control}
          />
        </Box>

        {uploadedFiles && Object.values(uploadedFiles).length ? (
          <Box my={3}>
            <List>
              {Object.keys(uploadedFiles).map((fileKey) => {
                // @ts-ignore
                const file = uploadedFiles[fileKey];
                return (
                  <ListItem key={file._id} disableGutters>
                    <ListItemAvatar>
                      <Avatar variant="rounded">
                        <StickyNote2SharpIcon />
                      </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary={file.name} secondary={moment().format('YYYY.MM.DD')} />
                    <ListItemSecondaryAction>
                      <IconButton onClick={() => setValue('uploadFiles', undefined)}>
                        <DeleteIcon />
                      </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>
                );
              })}
            </List>

            <Divider />
          </Box>
        ) : null}
        {initialValues?.files?.length ? (
          <Box my={3}>
            <List>
              {initialValues?.files.map((file, index) => {
                const avatarProps = {};
                if (file.fileType.includes('image')) {
                  // @ts-ignore
                  avatarProps.src = file.url;
                } else {
                  // @ts-ignore
                  avatarProps.children = <StickyNote2SharpIcon />;
                }
                return (
                  <ListItem
                    // @ts-ignore
                    key={file?._id + index}
                    disableGutters
                    component="a"
                    href={file.url}
                    target="_blank"
                  >
                    <ListItemAvatar>
                      <Avatar variant="rounded" {...avatarProps} />
                    </ListItemAvatar>
                    <ListItemText
                      primary={file.originalName}
                      secondary={moment(file.createdAt).format('YYYY.MM.DD')}
                    />
                    {onDeleteCmsSectionFile && (
                      <ListItemSecondaryAction>
                        <IconButton onClick={() => onDeleteCmsSectionFile(index)}>
                          <DeleteIcon />
                        </IconButton>
                      </ListItemSecondaryAction>
                    )}
                  </ListItem>
                );
              })}
            </List>
          </Box>
        ) : null}
      </DialogContent>
      {isLoading && <LinearProgress />}
      <DialogActions>
        <Button onClick={onClose} color="secondary" disabled={isLoading} sx={{ mr: 1 }}>
          Anulare
        </Button>
        <Button variant="contained" color="secondary" component="label">
          Adaugă fișiere
          <input hidden multiple type="file" {...register('uploadFiles')} />
        </Button>

        <Button variant="contained" type="submit" disabled={isLoading}>
          {initialValues?._id ? 'Modifică' : 'Crează'}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default CmsSectionForm;
