import React from 'react';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { VIEWPORT_MAX_WIDTH } from '../index';

const ProjectHeader = () => {
  return (
    <Box
      component="footer"
      sx={{ pt: 6, pb: 8, borderTop: `1px solid #ccc`, borderTopColor: 'divider' }}
    >
      <Container maxWidth={VIEWPORT_MAX_WIDTH}>
        <Grid container spacing={2} sx={{ my: 3, alignItems: 'center' }}>
          <Grid item xs={3}>
            <img
              // @ts-ignore
              src={`/dark-${process.env.NEXT_PUBLIC_LOGO.replace('/', '')}`}
              alt="logo"
              style={{ maxHeight: '90px', width: 'auto' }}
            />
          </Grid>
          <Grid item xs={6} sx={{ textAlign: 'center' }}>
            <Typography variant="body2">
              Proiect cofinanțat din Fondul Social European prin Programul Operațional Capital Uman
              2014-2020 Axa prioritară 3: Locuri de muncă pentru toți!
            </Typography>
            <Typography variant="body2">
              Competențe digitale și TIC pentru angajații din Regiunea Vest
            </Typography>
            <Typography variant="body2">Cod proiect: POCU/860/3/12/143523</Typography>
          </Grid>
          <Grid item xs={3}>
            <Box
              sx={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'flex-end',
              }}
            >
              <img
                src={`/logo-provitam.png`}
                alt="logo"
                style={{ maxHeight: '90px', width: 'auto' }}
              />
            </Box>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default ProjectHeader;
