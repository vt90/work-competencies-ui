import React from 'react';

const ProjectHeader = () => {
  return (
    <>
      <img src={`/ccih-header.jpeg`} alt="header" style={{ height: 'auto', width: '100%' }} />
    </>
  );
};

export default ProjectHeader;
