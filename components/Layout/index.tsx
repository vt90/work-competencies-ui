import React from 'react';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import dynamic from 'next/dynamic';
import { Breakpoint } from '@mui/system';
import AppFooter from './Footer';
import ProjectFooter from './ProjectFooter';
import ProjectHeader from './ProjectHeader';

const AppToolbar = dynamic(() => import('./Toolbar'), {
  ssr: false,
});

interface ILayoutProps {
  children: React.ReactNode;
}

export const VIEWPORT_MAX_WIDTH: Breakpoint = 'lg';

const Layout = (props: ILayoutProps) => {
  const { children } = props;
  const isHunedoara = process.env.NEXT_PUBLIC_TENANT === 'CCHUNEDOARA';

  return (
    <>
      {isHunedoara ? (
        <Box sx={{ bgcolor: '#FFF' }}>
          <Container maxWidth={VIEWPORT_MAX_WIDTH}>
            <ProjectHeader />
          </Container>
        </Box>
      ) : null}
      <AppToolbar />

      <Container
        sx={{ position: 'relative', minHeight: 'calc(100vh - 130px)' }}
        maxWidth={VIEWPORT_MAX_WIDTH}
      >
        <Box py={4}>{children}</Box>
      </Container>
      {isHunedoara ? <ProjectFooter /> : <AppFooter />}
    </>
  );
};

export default Layout;
