import React from 'react';
import Typography from '@mui/material/Typography';
import NavBreadcrumbs, { BreadcrumbInfo } from '../Breadcrumbs';

interface IPageHeaderProps {
  pages: BreadcrumbInfo[];
  title: string | React.ReactNode;
}

const PageHeader = (props: IPageHeaderProps) => {
  const { pages, title } = props;

  return (
    <>
      <NavBreadcrumbs pages={pages} />

      {typeof title === 'string' ? (
        <Typography variant="h5" component="h1">
          <strong>{title}</strong>
        </Typography>
      ) : (
        title
      )}
    </>
  );
};

export default PageHeader;
