import React, { useEffect } from 'react';
import Link from 'next/link';
import { signIn, signOut } from 'next-auth/react';
import Login from '@mui/icons-material/Login';
import Logout from '@mui/icons-material/Logout';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import ListItemText from '@mui/material/ListItemText';
import ListItemIcon from '@mui/material/ListItemIcon';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import capitalize from '@mui/utils/capitalize';
import { VIEWPORT_MAX_WIDTH } from '../index';
import { useAuthContext } from '../../../context/authContext';
import {
  ORG_ADMIN_PREFIX,
  ORG_USER_PREFIX,
  isOrgAdmin,
  isSuperAdmin,
  isOrgUser,
} from '../../../models/user';
import { useRouter } from 'next/router';
import { USER_PAGES } from '../../../models/userPages';

const AppToolbar = () => {
  const { user, organization, setOrganization } = useAuthContext();
  const [accountAnchorEl, setAccountAnchorEl] = React.useState<null | HTMLElement>(null);
  const router = useRouter();

  useEffect(() => {
    // @ts-ignore
    user?.userRoles?.length && setOrganization(user?.userRoles?.[0]?.name);
  }, [setOrganization, user]);

  const onChangeOrg = (event: SelectChangeEvent) => {
    setOrganization(event.target.value);
    router.push('/');
  };

  const getRoleDisplayName = (role: string): string => {
    let displayRole = '';
    if (isSuperAdmin(role)) displayRole = 'Platform admin';
    if (isOrgAdmin(role)) displayRole = role.substring(ORG_ADMIN_PREFIX.length);
    if (isOrgUser(role)) displayRole = role.substring(ORG_USER_PREFIX.length);
    return displayRole;
  };

  const accountOpen = Boolean(accountAnchorEl);
  const handleAccountClick = (event: React.MouseEvent<HTMLElement>) => {
    setAccountAnchorEl(event.currentTarget);
  };
  const handleAccountClose = () => {
    setAccountAnchorEl(null);
  };

  const getMenuItemColor = (menuItemUrl: string) => {
    return router.pathname.includes(menuItemUrl) ? 'info.light' : 'text.secondary';
  };

  return (
    <>
      <Box sx={{ p: 0.5, bgcolor: 'info.dark' }}>
        <Container maxWidth={VIEWPORT_MAX_WIDTH}>
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Box sx={{ my: 1, mr: 1 }}>
              <Link href="/">
                <a>
                  <img
                    src={process.env.NEXT_PUBLIC_LOGO}
                    alt="logo"
                    style={{ maxHeight: '32px', width: 'auto' }}
                  />
                </a>
              </Link>
            </Box>
            <Typography sx={{ color: '#FFFFFF', flexGrow: 1 }}>
              {process.env.NEXT_PUBLIC_TENANT_NAME}
            </Typography>
            {user ? null : (
              <Typography sx={{ color: '#FFFFFF' }} onClick={() => signIn()}>
                <Login /> Login
              </Typography>
            )}
          </Box>
        </Container>
      </Box>
      {user ? (
        <Box component="header">
          <Container maxWidth={VIEWPORT_MAX_WIDTH}>
            <Toolbar disableGutters>
              {
                // @ts-ignore
                user && organization && user?.userRoles?.length > 1 ? (
                  <Select
                    value={organization}
                    onChange={onChangeOrg}
                    size="small"
                    sx={{
                      boxShadow: 'none',
                      '.MuiOutlinedInput-notchedOutline': {
                        border: 0,
                        outline: 'none',
                      },
                    }}
                  >
                    {
                      // @ts-ignore
                      user?.userRoles.map((role) => (
                        <MenuItem key={role._id} value={role.name}>
                          {getRoleDisplayName(role.name)}
                        </MenuItem>
                      ))
                    }
                  </Select>
                ) : (
                  <Link href="/">
                    <a>
                      <Typography component="span">
                        <strong>{getRoleDisplayName(organization)}</strong>
                      </Typography>
                    </a>
                  </Link>
                )
              }

              <Box sx={{ flexGrow: 1 }} />
              {USER_PAGES.map((menuItem) => {
                if (!menuItem.isAvailable(organization)) return null;

                return (
                  <Box key={menuItem.name} px={1}>
                    <Link href={menuItem.url}>
                      <a>
                        <Typography component="span" sx={{ color: getMenuItemColor(menuItem.url) }}>
                          {capitalize(menuItem.name)}
                        </Typography>
                      </a>
                    </Link>
                  </Box>
                );
              })}
              <Divider orientation="vertical" flexItem sx={{ m: 2 }} />
              {/* @ts-ignore */}

              {user ? (
                <>
                  <IconButton onClick={handleAccountClick} size="small">
                    <Avatar sx={{ width: 42, height: 42, bgcolor: 'text.secondary' }}>
                      {/* @ts-ignore */}
                      {user?.name
                        ?.split(' ')
                        .splice(0, 2)
                        .map((name: string) => name.charAt(0).toUpperCase())
                        .join('.')}
                    </Avatar>
                  </IconButton>

                  <Menu
                    anchorEl={accountAnchorEl}
                    open={accountOpen}
                    onClose={handleAccountClose}
                    onClick={handleAccountClose}
                    PaperProps={{
                      elevation: 0,
                      sx: {
                        overflow: 'visible',
                        filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                        mt: 1.5,
                        '& .MuiAvatar-root': {
                          width: 32,
                          height: 32,
                          ml: -0.5,
                          mr: 1,
                        },
                        '&:before': {
                          content: '""',
                          display: 'block',
                          position: 'absolute',
                          top: 0,
                          right: 14,
                          width: 10,
                          height: 10,
                          bgcolor: 'background.paper',
                          transform: 'translateY(-50%) rotate(45deg)',
                          zIndex: 0,
                        },
                      },
                    }}
                    transformOrigin={{ horizontal: 'right', vertical: 'top' }}
                    anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
                  >
                    <MenuItem>
                      {/* @ts-ignore */}
                      <ListItemText primary={user.name} secondary={user.email} />
                    </MenuItem>
                    <Divider />
                    <MenuItem onClick={() => signOut()}>
                      <ListItemIcon>
                        <Logout fontSize="small" />
                      </ListItemIcon>
                      Logout
                    </MenuItem>
                  </Menu>
                </>
              ) : null}
            </Toolbar>

            <Divider />
          </Container>
        </Box>
      ) : null}
    </>
  );
};

export default AppToolbar;
