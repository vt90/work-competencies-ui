import React from 'react';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import LinearProgress from '@mui/material/LinearProgress';
import Rating from '@mui/material/Rating';
import Typography from '@mui/material/Typography';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { ICourseReview } from '../../../models/courseReview';

interface ICourseReviewFormProps {
  isLoading: boolean;
  open: boolean;
  onClose: () => void;
  onSubmit: (data: ICourseReview) => void;
  initialValues?: ICourseReview;
}
const schema = yup
  .object({
    review: yup.string().required(),
    rating: yup.number().required().min(1),
  })
  .required();

const CourseReviewForm = (props: ICourseReviewFormProps) => {
  const { isLoading, open, onClose, onSubmit, initialValues } = props;
  const {
    control,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: { ...initialValues },
    mode: 'onTouched',
    resolver: yupResolver(schema),
    shouldUnregister: true,
  });

  return (
    <Dialog
      open={open}
      keepMounted={false}
      onClose={onClose}
      // @ts-ignore
      PaperComponent="form"
      maxWidth="sm"
      fullWidth
      onSubmit={handleSubmit(onSubmit)}
    >
      <DialogTitle>
        <Typography textAlign="center" variant="h5" component="span" sx={{ display: 'block' }}>
          Lasă-ți părerea
        </Typography>
      </DialogTitle>
      <DialogContent>
        <Box mt={1} mb={3}>
          <Box mb={3} sx={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
            <input type="hidden" {...register(`_id`)} />
            <Controller
              name="rating"
              control={control}
              render={({ field }) => {
                const { value, onChange } = field;

                return (
                  <>
                    <Rating
                      size="large"
                      defaultValue={value || 0}
                      onChange={(event, newValue) => {
                        onChange(newValue);
                      }}
                    />
                  </>
                );
              }}
            />
          </Box>

          <TextField
            fullWidth
            error={!!errors.review}
            label="Părere"
            required={true}
            disabled={isLoading}
            helperText={!!errors.review && 'Părerea este necesara'}
            defaultValue={initialValues?.review || ''}
            {...register('review')}
          />
        </Box>
      </DialogContent>
      {isLoading && <LinearProgress />}
      <DialogActions sx={{ py: 2, justifyContent: 'center', bgcolor: 'background.default' }}>
        <Button color="inherit" onClick={onClose} disabled={isLoading}>
          Anulare
        </Button>
        <Button variant="contained" type="submit" disabled={isLoading}>
          Salveează
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default CourseReviewForm;
