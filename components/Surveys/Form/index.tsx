import React from 'react';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { ISurvey } from '../../../models/survey';

interface ISurveysFormProps {
  isLoading: boolean;
  initialValues: ISurvey;
  open: boolean;
  onClose: () => void;
  onSubmit: (data: any) => void;
}
const schema = yup
  .object({
    name: yup.string().required(),
    description: yup.string().required(),
  })
  .required();

const SurveysForm = (props: ISurveysFormProps) => {
  const { isLoading, initialValues, open, onClose, onSubmit } = props;
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: initialValues,
    mode: 'onTouched',
    resolver: yupResolver(schema),
    shouldUnregister: true,
  });

  const isUpdate = !!initialValues._id;

  return (
    <Dialog
      open={open}
      fullWidth={true}
      maxWidth="md"
      keepMounted={false}
      onClose={onClose}
      // @ts-ignore
      PaperComponent="form"
      onSubmit={handleSubmit(onSubmit)}
    >
      <DialogTitle>{isUpdate ? 'Modifică chestionar' : 'Creează chestionar nou'}</DialogTitle>
      <DialogContent dividers>
        <Box my={3}>
          <TextField
            fullWidth
            error={!!errors.name}
            label="Numele chestionarului"
            required={true}
            helperText={!!errors.name && 'Numele este necesar'}
            {...register('name')}
          />
        </Box>

        <Box my={3}>
          <TextField
            fullWidth
            error={!!errors.description}
            label="Scurta descriere"
            required={true}
            helperText={!!errors.description && 'Descrierea este necesar'}
            multiline
            rows={5}
            {...register('description')}
          />
        </Box>
      </DialogContent>
      <DialogActions>
        <Button disabled={isLoading} onClick={onClose}>
          Anulare
        </Button>
        <Button disabled={isLoading} variant="contained" type="submit">
          {isUpdate ? 'Modifică' : 'Crează'}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default SurveysForm;
