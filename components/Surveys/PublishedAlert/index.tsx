import React from 'react';
import Tooltip from '@mui/material/Tooltip';
import { ISurvey } from '../../../models/survey';
import YesNoToggle from '../../Common/YesNoToggle';

interface ISurveyPublishedAlertProps {
  survey: ISurvey;
  isLoading: boolean;
  onChange: (val: boolean) => void;
}

const SurveyPublishedAlert = (props: ISurveyPublishedAlertProps) => {
  const { survey, isLoading, onChange } = props;

  return (
    <Tooltip
      title={
        survey.published
          ? 'Acest chestionar este publicat și poate primi răspunsuri!'
          : 'Acest chestionar nu este publicat și nu poate primi răspunsuri!'
      }
    >
      <div>
        <YesNoToggle
          label="Publicat"
          value={!!survey.published}
          onValueChange={() => onChange(!survey.published)}
          disabled={isLoading}
          size="small"
        />
      </div>
    </Tooltip>
  );
};

export default SurveyPublishedAlert;
