import { ISurvey } from '../../models/survey';
import groupBy from 'lodash/groupBy';

export const getSurveyStats = (survey: ISurvey) => {
  const groupedByUser = groupBy(survey?.surveyAnswers, 'userId');
  const startedAnswering = Object.keys(groupedByUser).length;
  const completedAnswering = Object.values(groupedByUser).reduce((acc, curr) => {
    if (curr.length === survey.questions?.length) {
      acc++;
    }
    return acc;
  }, 0);

  return {
    startedAnswering,
    completedAnswering,
  };
};
