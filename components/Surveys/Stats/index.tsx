import React from 'react';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import { ISurvey } from '../../../models/survey';
import { getSurveyStats } from '../utils';

interface ISurveyStatsProps {
  survey: ISurvey;
}

const SurveyStats = (props: ISurveyStatsProps) => {
  const { survey } = props;
  const surveyStats = getSurveyStats(survey);

  const rows = [
    { text: 'Chestionare începute', value: surveyStats.startedAnswering },
    { text: 'Chestionare finalizate', value: surveyStats.completedAnswering },
  ];

  return (
    <Grid container sx={{ mb: 4 }} spacing={3} columns={10}>
      {rows.map((row) => (
        <Grid key={row.text} item xs={5} sm={5} md={3} lg={2}>
          <Card sx={{ py: 2, px: 3 }}>
            <Typography variant="body2" color="textSecondary" gutterBottom>
              {row.text}
            </Typography>
            <Typography variant="h4" fontWeight="500">
              {row.value}
            </Typography>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
};

export default SurveyStats;
