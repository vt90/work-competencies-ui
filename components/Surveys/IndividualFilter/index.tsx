import React, { useState, useMemo } from 'react';
import orderBy from 'lodash/orderBy';
import { ISurvey } from '../../../models/survey';
import { ISurveyAnswer, ISurveyAnswerWithProgress } from '../../../models/surveyAnswer';
import SurveyQuestionAnswerUsers from '../../SurveyQuestionAnswer/AnswerUsers';
import CloseIcon from '@mui/icons-material/Close';
import SearchIcon from '@mui/icons-material/Search';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import InputAdornment from '@mui/material/InputAdornment';
import MenuItem from '@mui/material/MenuItem';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import NoDataAvailable from '../../Common/NoDataAvailable';
import { IQuestion } from '../../../models/question';
import { SURVEY_TYPE } from '../../../services/surveys';

interface ISurveyIndividualFilterProps {
  survey: ISurvey;
  selectedIndividual: string;
  onSelectAnswerUser: (answer: ISurveyAnswer) => void;
}

const ORDER_BY = {
  name: {
    name: 'alfabetic',
    comparator: (answer: ISurveyAnswer) => answer.userName || answer.userEmail,
    order: 'asc',
  },
  userAnsweredQuestions: {
    name: 'progres',
    comparator: (answer: ISurveyAnswerWithProgress) => answer.userAnsweredQuestions,
    order: 'desc',
  },
  result: {
    name: 'rezultat',
    comparator: (answer: ISurveyAnswerWithProgress) => answer.result,
    order: 'desc',
  },
};

// ToDo optimize this component and array iterations
const SurveyIndividualFilter = (props: ISurveyIndividualFilterProps) => {
  const { survey, selectedIndividual, onSelectAnswerUser } = props;
  const [orderCriteria, setOrderCriteria] = useState('name');
  const [search, setSearch] = useState('');

  const onChangeCriteria = (event: SelectChangeEvent) => {
    setOrderCriteria(event.target.value);
  };

  const surveyCorrectAnswers = useMemo(() => {
    return survey.questions?.reduce((acc, question: IQuestion) => {
      const correctAnswer = question.answers?.find((answer) => answer.isCorrectAnswer);

      // @ts-ignore
      return { ...acc, [question._id]: correctAnswer?._id };
    }, {});
  }, [survey]);

  // @ts-ignore
  const surveyUsers: ISurveyAnswerWithProgress[] = useMemo(() => {
    const answersGroupedByUser = survey.surveyAnswers?.reduce((acc: ISurveyAnswer, cur) => {
      const key = cur.userId;

      // @ts-ignore
      if (!acc[key]) acc[key] = { ...cur, userAnsweredQuestions: 0, result: 0 };

      // @ts-ignore
      acc[key].userAnsweredQuestions++;

      if (
        surveyCorrectAnswers &&
        // @ts-ignore
        surveyCorrectAnswers[cur.questionId] &&
        // @ts-ignore
        surveyCorrectAnswers[cur.questionId] === cur.answer
      ) {
        // @ts-ignore
        acc[key].result++;
      }

      return acc;
    }, {});

    // @ts-ignore
    const usersGroup = Object.values(answersGroupedByUser).map((value) => ({
      // @ts-ignore
      ...value,
      // @ts-ignore
      ...(survey.type === SURVEY_TYPE.QUIZ && {
        // @ts-ignore
        displayResult: `(${value.result}/${survey.questions.length})`,
      }),
    }));

    return usersGroup;
  }, [survey, surveyCorrectAnswers]);

  const orderedUsers = useMemo(() => {
    // @ts-ignore
    return orderBy(
      surveyUsers,
      // @ts-ignore
      [ORDER_BY[orderCriteria].comparator],
      // @ts-ignore
      [ORDER_BY[orderCriteria].order],
    );
  }, [surveyUsers, orderCriteria]);

  const displayUsers = useMemo(() => {
    return orderedUsers.filter((answer) => {
      const displayName = answer.userName || answer.userEmail;

      return displayName?.toLowerCase().includes(search.toLowerCase());
    });
  }, [orderedUsers, search]);

  const selectedAnswer = useMemo(
    () => surveyUsers?.find((answer) => answer.userId === selectedIndividual),
    [selectedIndividual, surveyUsers],
  );

  const userDisplayName = selectedAnswer?.userName || selectedAnswer?.userEmail || (
    <NoDataAvailable content={`Nicio persoană selectată`} />
  );

  if (!displayUsers?.length) {
    return <NoDataAvailable content={`Nu există niciun răspuns dat chestionarului`} />;
  }

  return (
    <>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          flexWrap: 'wrap',
          mb: 2,
          bordercolor: 'divider',
        }}
      >
        <Box sx={{ flexGrow: 1 }}>
          <TextField
            value={search}
            onChange={(ev) => setSearch(ev.target.value)}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>
              ),
              ...(search && {
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton onClick={() => setSearch('')}>
                      <CloseIcon />
                    </IconButton>
                  </InputAdornment>
                ),
              }),
            }}
            size="small"
            variant="outlined"
            sx={{
              '& legend': { display: 'none' },
              '& fieldset': { top: 0 },
            }}
          />
        </Box>
        <Box sx={{ display: 'flex', alignItems: 'center' }}>
          <Typography variant="body2" color="textSecondary">
            Ordonează după
          </Typography>

          <Select
            value={orderCriteria}
            onChange={onChangeCriteria}
            size="small"
            sx={{
              boxShadow: 'none',
              '.MuiOutlinedInput-notchedOutline': {
                border: 0,
                outline: 'none',
              },
            }}
          >
            {
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore
              Object.keys(ORDER_BY).map((criteria: string) => (
                <MenuItem key={criteria} value={criteria}>
                  {
                    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                    // @ts-ignore
                    ORDER_BY[criteria].name
                  }
                </MenuItem>
              ))
            }
          </Select>
        </Box>
      </Box>
      <Box mb={3}>
        <SurveyQuestionAnswerUsers
          answers={displayUsers}
          onClick={onSelectAnswerUser}
          selectedAnswer={selectedAnswer}
        />
      </Box>

      <Box mb={3}>
        <Typography variant="h5">{userDisplayName}</Typography>
      </Box>
    </>
  );
};

export default SurveyIndividualFilter;
