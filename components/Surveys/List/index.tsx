import React, { useCallback, useMemo } from 'react';
import Link from 'next/link';
import moment from 'moment';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import IconButton from '@mui/material/IconButton';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import { ISurvey } from '../../../models/survey';
import { getSurveyStats } from '../utils';
import EnhancedTable from '../../Common/Table';
import Status from '../../Common/Status';

interface ISurveysListProps {
  baseUrl: string;
  surveys: ISurvey[];
  onSelectSurvey?: (org: ISurvey) => void;
  onDeleteSurvey?: (org: ISurvey) => void;
  readonly?: boolean;
}

const SurveysList = (props: ISurveysListProps) => {
  const { baseUrl, surveys, onSelectSurvey, onDeleteSurvey, readonly } = props;

  const headerRows = useMemo(() => {
    const headerItems = [
      { id: 'name', label: 'Chestionar', align: 'left', colSpan: 3, hideOnSmallDevice: true },
      ...(readonly
        ? [
            { id: 'questionsCount', label: '#Întrebări' },
            { id: 'answersCount', label: '#Răspunsuri' },
            { id: 'createdAt', label: 'Ultima modificare' },
          ]
        : [
            { id: 'published', label: 'Status' },
            { id: 'surveysStarted', label: '#Începute' },
            { id: 'surveysCompleted', label: '#Completate' },
            { id: 'createdAt', label: 'Ultima modificare' },
          ]),
    ];

    if (!readonly && (onSelectSurvey || onDeleteSurvey)) {
      // @ts-ignore
      headerItems.push({ id: 'actions', label: 'Acțiuni', align: 'right', disableSorting: true });
    }

    return headerItems;
  }, [onDeleteSurvey, onSelectSurvey, readonly]);

  const getHeaderRows = useCallback(() => {
    return surveys.map((survey) => {
      const surveyStats = getSurveyStats(survey);

      const renderName = (
        <Link href={`${baseUrl}/${survey._id}`}>
          <Typography component="a">{survey.name}</Typography>
        </Link>
      );

      return {
        _id: survey._id,
        name: renderName,
        smallDeviceHeader: renderName,
        questionsCount: survey?.questions?.length,
        answersCount: survey?.surveyAnswers?.length,
        surveysStarted: surveyStats.startedAnswering,
        surveysCompleted: surveyStats.completedAnswering,
        createdAt: moment(survey.createdAt).format('YYYY.MM.DD'),
        published: (
          <Status
            label={survey.published ? 'Publicat' : 'Nepublicat'}
            color={survey.published ? 'info' : 'warning'}
            tooltipText={
              survey.published
                ? 'Acest chestionar este publicat și poate primi răspunsuri!'
                : 'Acest chestionar nu este publicat și nu poate primi răspunsuri!'
            }
          />
        ),
        actions: (
          <>
            {onSelectSurvey && (
              <IconButton onClick={() => onSelectSurvey(survey)} size="small">
                <EditIcon fontSize="inherit" />
              </IconButton>
            )}

            {onDeleteSurvey && (
              <IconButton onClick={() => onDeleteSurvey(survey)} size="small">
                <DeleteIcon fontSize="inherit" />
              </IconButton>
            )}
          </>
        ),
        data: survey,
      };
    });
  }, [baseUrl, onDeleteSurvey, onSelectSurvey, surveys]);

  return (
    <>
      <EnhancedTable
        headerRows={headerRows}
        orderByFieldCriteriaFunctions={{
          answersCount: (row: any) => row?.data && row.data.surveyAnswers?.length,
          questionsCount: (row: any) => row?.data && row.data.questions?.length,
          surveysStarted: (row: any) => row?.data && getSurveyStats(row.data).completedAnswering,
          surveysCompleted: (row: any) => row?.data && getSurveyStats(row.data).completedAnswering,
          published: (row: any) => row?.data && !!row.data.published,
        }}
        PaperComponent={Card}
        paperComponentStyles={{
          px: 2,
          py: { xs: 2, md: 3 },
        }}
        rows={getHeaderRows()}
        smallUntilBreakpoint="md"
      />
    </>
  );
};

export default SurveysList;
