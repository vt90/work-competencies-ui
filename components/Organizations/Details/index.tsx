import React from 'react';
import { useQuery } from '@tanstack/react-query';
import { IOrganization } from '../../../models/organization';
import { getOrganizationUsersByOrganizationId } from '../../../services/organizations';
import UsersList from '../../Users/List';
import ListLoadingIndicator from '../../Common/ListLoadingIndcator';

interface IOrganizationDetailsProps {
  organisation: IOrganization;
}

const OrganizationDetails = (props: IOrganizationDetailsProps) => {
  const { organisation } = props;

  const { isLoading, data: organizationUsers } = useQuery(
    ['organization-users', organisation?._id],
    // @ts-ignore
    () => getOrganizationUsersByOrganizationId(organisation._id),
    {
      enabled: !!organisation._id,
    },
  );

  return (
    <>
      {isLoading && <ListLoadingIndicator />}
      {organizationUsers && <UsersList users={organizationUsers} />}
    </>
  );
};

export default OrganizationDetails;
