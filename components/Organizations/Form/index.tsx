import React from 'react';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import LinearProgress from '@mui/material/LinearProgress';
import { IOrganization } from '../../../models/organization';

interface IOrganizationsFormProps {
  defaultValues: Partial<IOrganization>;
  isLoading: boolean;
  open: boolean;
  onClose: () => void;
  onSubmit: (data: any) => void;
}
const schema = yup
  .object({
    name: yup.string().required(),
  })
  .required();

const OrganizationsForm = (props: IOrganizationsFormProps) => {
  const { defaultValues, isLoading, open, onClose, onSubmit } = props;
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: defaultValues || { name: '' },
    mode: 'onTouched',
    resolver: yupResolver(schema),
    shouldUnregister: true,
  });

  return (
    <Dialog
      open={open}
      keepMounted={false}
      onClose={onClose}
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      PaperComponent="form"
      maxWidth="sm"
      onSubmit={handleSubmit(onSubmit)}
    >
      <DialogTitle>
        {defaultValues._id ? 'Modifică organizație' : 'Creează organizație nouă'}{' '}
      </DialogTitle>
      <DialogContent>
        <input type="hidden" {...register(`_id`)} />
        <Box my={3}>
          <TextField
            fullWidth
            error={!!errors.name}
            label="Denumire"
            required={true}
            disabled={isLoading}
            helperText={!!errors.name && 'Denumirea este necesara'}
            defaultValue=""
            {...register('name')}
          />
        </Box>
      </DialogContent>
      {isLoading && <LinearProgress />}
      <DialogActions sx={{ py: 2, justifyContent: 'center', bgcolor: 'background.default' }}>
        <Button onClick={onClose} disabled={isLoading}>
          Anulare
        </Button>
        <Button variant="contained" type="submit" disabled={isLoading}>
          {defaultValues._id ? 'Modifică' : 'Creează'}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default OrganizationsForm;
