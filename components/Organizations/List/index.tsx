import React, { useMemo } from 'react';
import orderBy from 'lodash/orderBy';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import Delete from '@mui/icons-material/Delete';
import Edit from '@mui/icons-material/Edit';
import { IOrganization } from '../../../models/organization';
import {
  isOrgAdmin,
  isOrgUser,
  isSuperAdmin,
  ORG_ADMIN_PREFIX,
  ORG_USER_PREFIX,
} from '../../../models/user';

interface IOrganizationsListProps {
  organizations: IOrganization[];
  onSelectOrganization: (org: IOrganization) => void;
  onDeleteOrganization: (org: IOrganization) => void;
  onEditOrganization: (org: IOrganization) => void;
}

const OrganizationsList = (props: IOrganizationsListProps) => {
  const { organizations, onDeleteOrganization, onEditOrganization, onSelectOrganization } = props;

  const getOrganizationTitle = (role: string): string => {
    let displayRole = '';
    if (isSuperAdmin(role)) displayRole = 'Administratori Platforma';
    if (isOrgAdmin(role)) displayRole = `${role.substring(ORG_ADMIN_PREFIX.length)}`;
    if (isOrgUser(role)) displayRole = `${role.substring(ORG_USER_PREFIX.length)}`;
    return displayRole;
  };

  const orderedOrganizations = useMemo(() => {
    return orderBy(
      organizations.filter((org) => !isOrgUser(org.name)),
      [
        (org) => {
          if (isSuperAdmin(org.name)) {
            return 'A';
          }

          return org.name.split('_')[1];
        },
      ],
      ['asc'],
    );
  }, [organizations]);

  return (
    <List>
      {orderedOrganizations.map((organization, index) => {
        const { _id, name } = organization;
        const divider = index < orderedOrganizations.length - 1;
        return (
          <ListItem
            key={_id}
            onClick={() => onSelectOrganization(organization)}
            disablePadding
            divider={divider}
          >
            <ListItemButton>
              <ListItemText
                sx={{
                  ...(isSuperAdmin(name) && {
                    fontStyle: 'italic',
                    color: 'text.secondary',
                  }),
                }}
                primary={getOrganizationTitle(name)}
              />
            </ListItemButton>
            <ListItemSecondaryAction>
              <IconButton
                disabled={isSuperAdmin(name)}
                onClick={() =>
                  onEditOrganization({
                    ...organization,
                    name: getOrganizationTitle(name),
                  })
                }
              >
                <Edit />
              </IconButton>

              <IconButton
                disabled={isSuperAdmin(name)}
                onClick={() =>
                  onDeleteOrganization({
                    ...organization,
                    name: getOrganizationTitle(name),
                  })
                }
              >
                <Delete />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        );
      })}
    </List>
  );
};

export default OrganizationsList;
