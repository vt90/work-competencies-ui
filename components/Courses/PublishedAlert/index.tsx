import React from 'react';
import Tooltip from '@mui/material/Tooltip';
import YesNoToggle from '../../Common/YesNoToggle';
import { ICourse } from '../../../models/course';

interface ICoursePublishedAlertProps {
  course: ICourse;
  isLoading: boolean;
  onChange: (val: boolean) => void;
}

const CoursePublishedAlert = (props: ICoursePublishedAlertProps) => {
  const { course, isLoading, onChange } = props;

  return (
    <>
      <Tooltip
        title={course.published ? 'Acest curs este publicat!' : 'Acest curs nu este publicat!'}
      >
        <div>
          <YesNoToggle
            label="Publicat"
            value={!!course.published}
            onValueChange={() => onChange(!course.published)}
            disabled={isLoading}
            size="small"
          />
        </div>
      </Tooltip>
    </>
  );
};

export default CoursePublishedAlert;
