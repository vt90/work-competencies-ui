import React from 'react';
import CircleOutlinedIcon from '@mui/icons-material/CircleOutlined';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import CancelIcon from '@mui/icons-material/Cancel';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import { IQuestion } from '../../../models/question';
import { getQuestionDisplayAnswers } from '../../Questions/utils';
interface ICourseQuestionResponseItemProps {
  question: IQuestion;
}

const CourseQuestionResponseItem = (props: ICourseQuestionResponseItemProps) => {
  const { question } = props;
  // @ts-ignore
  const userAnswer = question?.userAnswer;

  // @ts-ignore
  const correctAnswer = question?.correctAnswer;

  return (
    <>
      <Card>
        <Box p={3}>
          <Typography variant="h6" sx={{ mb: 2 }}>
            {question.text}
          </Typography>

          {/* @ts-ignore */}
          {getQuestionDisplayAnswers(question).map((answer) => {
            const { _id, text } = answer;

            let color = 'text.disabled';
            let icon = <CircleOutlinedIcon sx={{ color }} />;
            let helperText = '';

            if (_id === correctAnswer?._id) {
              color = 'success.main';
              icon = <CheckCircleIcon sx={{ color }} />;
              // @ts-ignore
              helperText = question?.isCorrectAnswer ? 'Răspuns corect' : 'Răspunsul corect';
            }

            // @ts-ignore
            if (_id === userAnswer?._id && !question.isCorrectAnswer) {
              color = 'error.main';
              icon = <CancelIcon sx={{ color }} />;
              helperText = 'Răspuns greşit';
            }

            return (
              <Box mb={0.5} key={text}>
                <Box my={1} sx={{ display: 'flex', alignItems: 'flex-start' }}>
                  <Box mr={1}>{icon}</Box>

                  <Box mt={0.5} sx={{ flexGrow: 1 }}>
                    <Typography sx={{ color }}>{text}</Typography>

                    {helperText && (
                      <Typography variant="caption" sx={{ color }}>
                        {helperText}
                      </Typography>
                    )}
                  </Box>
                </Box>
              </Box>
            );
          })}
        </Box>
      </Card>
    </>
  );
};

export default CourseQuestionResponseItem;
