import React from 'react';
import StickyNote2SharpIcon from '@mui/icons-material/StickyNote2Sharp';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { ICourse } from '../../../models/course';
import { ISurvey } from '../../../models/survey';
import QuestionsResponseList from '../../Questions/ResponseList';
import { ISurveyAnswer } from '../../../models/surveyAnswer';

interface ICourseTestQAProps {
  course: ICourse;
  courseSurvey?: ISurvey;
  isLoading: boolean;
  answeringQuestionId: string | undefined;
  onAnswerChange: (answer: Partial<ISurveyAnswer>) => void;
}

const CourseTestQA = (props: ICourseTestQAProps) => {
  const { answeringQuestionId, courseSurvey, onAnswerChange } = props;

  return (
    <>
      {courseSurvey?.published ? (
        <QuestionsResponseList
          answeringQuestionId={answeringQuestionId}
          // @ts-ignore
          questions={courseSurvey.questions}
          // @ts-ignore
          surveyAnswers={courseSurvey.surveyAnswers}
          onAnswerChange={onAnswerChange}
        />
      ) : (
        <Grid container justifyContent="center" sx={{ bgcolor: 'divider', py: 2 }}>
          <Grid item xs={9} sm={5}>
            <Box sx={{ display: 'flex', justifyContent: 'center', my: 2 }}>
              <StickyNote2SharpIcon
                sx={{
                  typography: 'h1',
                  color: 'text.disabled',
                  textAlign: 'center',
                }}
              />
            </Box>

            <Typography color="textSecondary" textAlign="center">
              Acest curs nu are niciun formular de evaluare!
            </Typography>
          </Grid>
        </Grid>
      )}
    </>
  );
};

export default CourseTestQA;
