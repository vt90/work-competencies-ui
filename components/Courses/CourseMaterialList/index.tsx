import React from 'react';
import StickyNote2SharpIcon from '@mui/icons-material/StickyNote2Sharp';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { ICourse, ICourseMaterial } from '../../../models/course';
import moment from 'moment';
import NoDataAvailable from '../../Common/NoDataAvailable';
import dynamic from 'next/dynamic';
import { COLORS } from '../../../lib/theme';
import Divider from '@mui/material/Divider';
import List from '@mui/material/List';
import { ListItem } from '@mui/material';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';

const RichText = dynamic(() => import('../../Common/RichText'), {
  ssr: false,
});

interface ICourseMaterialListProps {
  course: ICourse;
  onSelectCourseMaterial?: (courseMaterial: ICourseMaterial) => void;
  onDeleteCourseMaterial?: (courseMaterial: ICourseMaterial) => void;
}

const HORIZONTAL_PADDING = {
  xs: 3,
  md: 15,
};

const VERTICAL_PADDING = {
  xs: 3,
  md: 7,
};

const CourseMaterialList = (props: ICourseMaterialListProps) => {
  const { course, onSelectCourseMaterial, onDeleteCourseMaterial } = props;

  if (!course?.courseMaterials?.length) {
    return <NoDataAvailable content="Nu există niciun material de curs!" />;
  }

  return (
    <Grid container spacing={2} justifyContent="center">
      {course?.courseMaterials.map((courseMaterial, index) => {
        const color = COLORS[index % COLORS.length];
        return (
          <Grid key={courseMaterial?._id} item xs={12} sx={{ my: 2 }}>
            <Card>
              <Box
                sx={{
                  background: color,
                  display: 'flex',
                  py: 1,
                  px: HORIZONTAL_PADDING,
                }}
              >
                <Typography variant="caption" sx={{ color: '#FFFFFF', my: 1, flexGrow: 1 }}>
                  {moment(courseMaterial.createdAt).format('YYYY.MM.DD')}
                </Typography>

                {onSelectCourseMaterial && (
                  <IconButton onClick={() => onSelectCourseMaterial(courseMaterial)}>
                    <EditIcon sx={{ color: '#FFFFFF' }} />
                  </IconButton>
                )}
                {onDeleteCourseMaterial && (
                  <IconButton onClick={() => onDeleteCourseMaterial(courseMaterial)}>
                    <DeleteIcon sx={{ color: '#FFFFFF' }} />
                  </IconButton>
                )}
              </Box>

              <Box
                sx={{
                  pt: VERTICAL_PADDING,
                  px: HORIZONTAL_PADDING,
                }}
              >
                <Typography variant="h5">{courseMaterial.title}</Typography>
              </Box>

              <Box
                sx={{
                  px: HORIZONTAL_PADDING,
                }}
              >
                {courseMaterial?.description && (
                  <Box pt={3} pb={VERTICAL_PADDING}>
                    <RichText
                      readOnly
                      defaultValue={courseMaterial.description}
                      sx={{
                        boxShadow: 0,
                        '& .ql-container': {
                          border: 'none !important',
                          minHeight: '0',
                          overflowY: 'visible',
                          p: 0,
                        },
                        '& .ql-editor': {
                          overflowY: 'visible',
                          p: 0,
                        },
                        '& .quill': {
                          overflowY: 'visible',
                        },
                      }}
                    />
                  </Box>
                )}
              </Box>

              {courseMaterial?.files?.length ? (
                <>
                  <Divider />

                  <Box
                    sx={{
                      px: HORIZONTAL_PADDING,
                    }}
                  >
                    <List>
                      {courseMaterial?.files.map((file) => {
                        const avatarProps = {};
                        if (file.fileType.includes('image')) {
                          // @ts-ignore
                          avatarProps.src = file.url;
                        } else {
                          // @ts-ignore
                          avatarProps.children = <StickyNote2SharpIcon />;
                        }
                        return (
                          <ListItem
                            key={file._id}
                            disableGutters
                            component="a"
                            href={file.url}
                            target="_blank"
                          >
                            <ListItemAvatar>
                              <Avatar variant="rounded" {...avatarProps} />
                            </ListItemAvatar>
                            <ListItemText
                              primary={file.originalName}
                              secondary={moment(file.createdAt).format('YYYY.MM.DD')}
                            />
                          </ListItem>
                        );
                      })}
                    </List>
                  </Box>
                </>
              ) : null}
            </Card>
          </Grid>
        );
      })}
    </Grid>
  );
};

export default CourseMaterialList;
