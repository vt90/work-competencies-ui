import React from 'react';
import Button from '@mui/lab/LoadingButton';
import { ICourse } from '../../../models/course';
import SurveysList from '../../Surveys/List';
import { ISurvey } from '../../../models/survey';
import NoDataAvailable from '../../Common/NoDataAvailable';

interface ICourseTestProps {
  course: ICourse;
  courseSurvey?: ISurvey;
  isLoading: boolean;
  readonly?: boolean;
  onCreateSurvey?: () => void;
  onDeleteSurvey?: () => void;
}

const CourseTest = (props: ICourseTestProps) => {
  const { course, courseSurvey, isLoading, onCreateSurvey, onDeleteSurvey } = props;

  if (!course) return null;

  return (
    <>
      {courseSurvey ? (
        <SurveysList
          baseUrl={`/management/courses/${course?._id}/survey`}
          surveys={[courseSurvey]}
          onDeleteSurvey={onDeleteSurvey}
        />
      ) : (
        <NoDataAvailable
          actions={
            <Button variant="contained" onClick={onCreateSurvey} loading={isLoading}>
              Crează
            </Button>
          }
          content="Acest curs nu are niciun formular de evaluare!"
        />
      )}
    </>
  );
};

export default CourseTest;
