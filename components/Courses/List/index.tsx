import React, { useCallback, useMemo } from 'react';
import Link from 'next/link';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import Card from '@mui/material/Card';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { ICourse } from '../../../models/course';
import moment from 'moment/moment';
import EnhancedTable from '../../Common/Table';
import Status from '../../Common/Status';

interface ICoursesListProps {
  baseUrl: string;
  courses: ICourse[];
  onSelectCourse?: (org: ICourse) => void;
  onDeleteCourse?: (org: ICourse) => void;
  readonly?: boolean;
}

const CoursesList = (props: ICoursesListProps) => {
  const { baseUrl, courses, onSelectCourse, onDeleteCourse, readonly } = props;

  const headerRows = useMemo(
    () => [
      { id: 'name', label: 'Chestionar', align: 'left', colSpan: 3, hideOnSmallDevice: true },
      ...(readonly
        ? [{ id: 'createdAt', label: 'Ultima modificare' }]
        : [
            { id: 'published', label: 'Status' },
            { id: 'createdAt', label: 'Ultima modificare' },
            { id: 'actions', label: 'Acțiuni', align: 'right', disableSorting: true },
          ]),
    ],
    [readonly],
  );

  const getHeaderRows = useCallback(() => {
    return courses.map((course) => {
      const renderName = (
        <Link href={`${baseUrl}/${course._id}`}>
          <Typography component="a">{course.title}</Typography>
        </Link>
      );

      return {
        _id: course._id,
        name: renderName,
        smallDeviceHeader: renderName,
        createdAt: moment(course.createdAt).format('YYYY.MM.DD'),
        published: (
          <Status
            label={course.published ? 'Publicat' : 'Nepublicat'}
            color={course.published ? 'info' : 'warning'}
            tooltipText={
              course.published ? 'Acest curs este publicat!' : 'Acest curs nu este publicat!'
            }
          />
        ),
        actions: (
          <>
            {onSelectCourse && (
              <IconButton onClick={() => onSelectCourse(course)} size="small">
                <EditIcon fontSize="inherit" />
              </IconButton>
            )}

            {onDeleteCourse && (
              <IconButton onClick={() => onDeleteCourse(course)} size="small">
                <DeleteIcon fontSize="inherit" />
              </IconButton>
            )}
          </>
        ),
        data: course,
      };
    });
  }, [baseUrl, onSelectCourse, onDeleteCourse, courses]);

  return (
    <>
      <EnhancedTable
        headerRows={headerRows}
        orderByFieldCriteriaFunctions={{
          published: (row: any) => row?.data && !!row.data.published,
        }}
        PaperComponent={Card}
        paperComponentStyles={{
          px: 2,
          py: { xs: 2, md: 3 },
        }}
        rows={getHeaderRows()}
        smallUntilBreakpoint="md"
      />
    </>
  );
};

export default CoursesList;
