import React, { useMemo } from 'react';
import { ICourse } from '../../../models/course';
import groupBy from 'lodash/groupBy';
import noop from 'lodash/noop';
import moment from 'moment';
import Avatar from '@mui/material/Avatar';
import Divider from '@mui/material/Divider';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Rating from '@mui/material/Rating';
import Typography from '@mui/material/Typography';
import PercentBar from '../../Common/Charts/PercentBar';
import { useTheme } from '@mui/material/styles';
import { COLORS } from '../../../lib/theme';
import SurveyQuestionAnswerUsers from '../../SurveyQuestionAnswer/AnswerUsers';
import Button from '@mui/lab/LoadingButton';
import NoDataAvailable from '../../Common/NoDataAvailable';

interface ICourseReviewsProps {
  course: ICourse;
  onSetReview?: () => void;
}

const CourseReviews = (props: ICourseReviewsProps) => {
  const { course, onSetReview } = props;
  const theme = useTheme();

  const groupedReviews = useMemo(
    () => course?.courseReviews && groupBy(course.courseReviews, 'rating'),
    [course],
  );

  if (!(course?.courseReviews?.length && course?.avgReview))
    return (
      <NoDataAvailable
        actions={
          onSetReview && (
            <Button variant="contained" onClick={onSetReview} loading={false}>
              Adaugă review
            </Button>
          )
        }
        content="Acest curs nu are niciun review!"
      />
    );

  return (
    <>
      <Box sx={{ display: 'flex', alignItems: 'center' }}>
        <Box mr={{ xs: 2, md: 5 }}>
          <Typography align="center" variant="h2" color="textSecondary">
            {course.avgReview}
          </Typography>

          <Box sx={{ display: 'flex', justifyContent: 'center' }}>
            <Rating readOnly value={course.avgReview} precision={0.5} />
          </Box>
        </Box>

        <Box sx={{ flexGrow: 1, maxWidth: 500, transform: 'scale(0.7)' }}>
          {Array(5)
            .fill(1)
            .map((_, index) => {
              const value = 5 - index;
              // @ts-ignore
              const reviewsNumber = groupedReviews[`${value}`]?.length || 0;
              // @ts-ignore
              const percent = (reviewsNumber / course?.courseReviews?.length) * 100;
              return (
                <Box key={value} mb={1}>
                  <PercentBar
                    color={theme.palette.text.secondary}
                    label={<Rating readOnly value={value} size="small" sx={{ mb: 1 }} />}
                    value={percent}
                  />
                </Box>
              );
            })}
        </Box>
      </Box>

      <Paper sx={{ px: 3, py: 2, mb: 5 }}>
        {course?.courseReviews?.map((review, index) => {
          return (
            <Box key={review._id}>
              <Box sx={{ display: 'flex' }} my={2}>
                <Box mr={1} mt={0.5}>
                  <Avatar
                    sx={{
                      background: COLORS[index % COLORS.length],
                    }}
                  >
                    {review.userName?.charAt(0).toUpperCase()}
                  </Avatar>
                </Box>

                <Box sx={{ flexGrow: 1 }}>
                  <Box sx={{ display: 'flex', alignItems: 'center', mb: 2 }}>
                    <SurveyQuestionAnswerUsers answers={[review]} onClick={noop} />

                    <Typography variant="caption" color="textSecondary">
                      {moment(review.updatedAt).format('YYYY.MM.DD HH:mm')}
                    </Typography>
                  </Box>

                  <Rating readOnly value={review.rating} size="small" sx={{ mr: 1 }} />

                  <Box sx={{ flexGrow: 1 }}>
                    <Typography gutterBottom>{review.review}</Typography>
                  </Box>
                </Box>
              </Box>

              {/* @ts-ignore */}
              {index < course?.courseReviews?.length - 1 && <Divider />}
            </Box>
          );
        })}
      </Paper>
    </>
  );
};

export default CourseReviews;
