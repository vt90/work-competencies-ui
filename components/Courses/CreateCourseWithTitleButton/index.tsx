import React, { useMemo, useState } from 'react';
import CreateIcon from '@mui/icons-material/Add';
import Box from '@mui/material/Box';
import Tooltip from '@mui/material/Tooltip';
import Typography from '@mui/material/Typography';
import Button from '@mui/lab/LoadingButton';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { createOrUpdateCourse, checkExisting } from '../../../services/courses';
import { useAuthContext } from '../../../context/authContext';
import Dialog from '../../Common/Dialog';
import { ICourse } from '../../../models/course';
import { useRouter } from 'next/router';

interface IPercentBarProps {
  title: string;
}

const CreateCourseWithTitleButton = (props: IPercentBarProps) => {
  const [showExistingDialog, setShowExistingDialog] = useState<Partial<ICourse>[] | null>(null);
  const { title } = props;
  const queryClient = useQueryClient();
  const { organization: orgId } = useAuthContext();
  const router = useRouter();

  const courseInfo = useMemo(
    () => ({
      title,
      orgId,
      content: '',
    }),
    [orgId, title],
  );

  const { isLoading: isChecking, mutate: check } = useMutation(checkExisting, {
    onSuccess: async (data) => {
      if (data?.length) {
        setShowExistingDialog(data);
      } else {
        create(courseInfo);
      }
    },
  });

  const { isLoading: isCreating, mutate: create } = useMutation(createOrUpdateCourse, {
    onSuccess: async (data) => {
      const { _id } = data;
      await queryClient.invalidateQueries(['courses']);
      await router.push(`/management/courses/${_id}`);
    },
  });

  return (
    <>
      <Tooltip title="Crează curs">
        <Button
          color="secondary"
          sx={{ minWidth: 0, padding: 0.5 }}
          onClick={() => check(courseInfo)}
          variant="contained"
          loading={isChecking || isCreating}
        >
          <CreateIcon fontSize="small" />
        </Button>
      </Tooltip>

      <Dialog
        actions={
          <Box ml={1}>
            <Button variant="contained" onClick={() => create(courseInfo)} loading={isCreating}>
              Crează
            </Button>
          </Box>
        }
        content={
          <>
            <Typography color="textSecondary" textAlign="center">
              Exista deja cursul
              <strong>
                <i>&quot;{title}&quot;</i>
              </strong>
              ! Doriti sa creati alt curs cu acelasi nume?
            </Typography>
          </>
        }
        title="Curs existent!"
        onClose={() => setShowExistingDialog(null)}
        open={!!showExistingDialog}
      />
    </>
  );
};

export default CreateCourseWithTitleButton;
