import React from 'react';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import LinearProgress from '@mui/material/LinearProgress';

interface ICourseCreateFormProps {
  isLoading: boolean;
  open: boolean;
  onClose: () => void;
  onSubmit: (data: any) => void;
}
const schema = yup
  .object({
    title: yup.string().required(),
  })
  .required();

const CourseCreateForm = (props: ICourseCreateFormProps) => {
  const { isLoading, open, onClose, onSubmit } = props;
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: { title: '' },
    mode: 'onTouched',
    resolver: yupResolver(schema),
    shouldUnregister: true,
  });

  return (
    <Dialog
      open={open}
      keepMounted={false}
      onClose={onClose}
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      PaperComponent="form"
      maxWidth="sm"
      onSubmit={handleSubmit(onSubmit)}
    >
      <DialogTitle>Creează curs nou</DialogTitle>
      <DialogContent dividers>
        <Box my={3}>
          <TextField
            fullWidth
            error={!!errors.title}
            label="Titlu"
            required={true}
            disabled={isLoading}
            helperText={!!errors.title && 'Titlul este necesar'}
            defaultValue=""
            {...register('title')}
          />
        </Box>
      </DialogContent>
      {isLoading && <LinearProgress />}
      <DialogActions>
        <Button onClick={onClose} disabled={isLoading}>
          Anulare
        </Button>
        <Button variant="contained" type="submit" disabled={isLoading}>
          Crează
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default CourseCreateForm;
