import React, { createContext, useContext, useEffect, useState } from 'react';
import { useSession } from 'next-auth/react';
import { IUser } from '../models/user';

const AuthContext = createContext({
  user: undefined,
  organization: '',
  accessToken: '',
  setOrganization: (value: any) => null,
});

interface IAuthWrapperProps {
  children: React.ReactNode;
}

export function AuthProvider(props: IAuthWrapperProps) {
  const [organization, setOrganization] = useState<string>('');
  const { children } = props;
  const { data: session } = useSession();
  // @ts-ignore
  const user: IUser | undefined = session?.user;
  // @ts-ignore
  const accessToken: string | undefined = session?.accessToken;

  useEffect(() => {
    localStorage.setItem('@WCUI_ORGANIZATION', organization);
  }, [organization]);

  return (
    // @ts-ignore
    <AuthContext.Provider value={{ accessToken, user, organization, setOrganization }}>
      {children}
    </AuthContext.Provider>
  );
}

export function useAuthContext() {
  return useContext(AuthContext);
}
