import axios from 'axios';
import { ICourseReview } from '../models/courseReview';

const httpInstance = axios.create({
  baseURL: '/api/course-reviews',
});

httpInstance.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error && error.response && error.response.data) {
      return Promise.reject(
        error.response.data.error ? error.response.data.error : error.response.data,
      );
    } else {
      return Promise.reject({ message: 'Server error' });
    }
  },
);

const cleanupCourseReviewBody = (course: Partial<ICourseReview>): Partial<ICourseReview> => {
  delete course._id;
  // @ts-ignore
  delete course.id;
  // @ts-ignore
  delete course.userId;
  // @ts-ignore
  delete course['__v'];

  return course;
};

export const createOrUpdateCourseReview = async (
  courseReview: Partial<ICourseReview>,
): Promise<ICourseReview> => {
  const isUpdate = !!courseReview._id;
  const url = isUpdate ? `/${courseReview._id}` : ``;
  const req = isUpdate ? httpInstance.patch : httpInstance.post;
  return await req(url, cleanupCourseReviewBody(courseReview));
};
