import axios, { AxiosInstance } from 'axios';
import { ICourse } from '../models/course';

const httpInstance = axios.create({
  baseURL: '/api/courses',
});

httpInstance.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error && error.response && error.response.data) {
      return Promise.reject(
        error.response.data.error ? error.response.data.error : error.response.data,
      );
    } else {
      return Promise.reject({ message: 'Server error' });
    }
  },
);

const cleanupCourseBody = (course: Partial<ICourse>): Partial<ICourse> => {
  delete course._id;
  // @ts-ignore
  delete course.id;
  // @ts-ignore
  delete course['__v'];
  // @ts-ignore
  delete course.courseReviews;
  // @ts-ignore
  delete course.courseMaterials;
  // @ts-ignore
  delete course.courseSurvey;
  // @ts-ignore
  delete course.avgReview;

  delete course.slug;

  return course;
};

export const getCourses = async (params: ICourse): Promise<ICourse[]> => {
  return await httpInstance.get('', {
    params: { ...params },
  });
};

export const getCoursesManagement = async (params: ICourse): Promise<ICourse[]> => {
  return await httpInstance.get('/management/find', {
    params: { ...params },
  });
};

export const getCourseById = async (id: string): Promise<ICourse> => {
  return await httpInstance.get(`/${id}`);
};

export const getCourseByIdManagement = async (id: string): Promise<ICourse> => {
  return await httpInstance.get(`/management/${id}`);
};

export const checkExisting = async (course: Partial<ICourse>): Promise<ICourse[]> => {
  return await httpInstance.post(`/management/check-existing`, course);
};

export const createOrUpdateCourse = async (course: Partial<ICourse>): Promise<ICourse> => {
  const isUpdate = !!course._id;
  const url = isUpdate ? `/${course._id}` : ``;
  const req = isUpdate ? httpInstance.patch : httpInstance.post;
  return await req(`/management${url}`, cleanupCourseBody(course));
};
export const deleteCourse = async (id: string): Promise<any> => {
  return await httpInstance.delete(`/management/${id}`);
};
