import axios, { AxiosInstance } from 'axios';
import { ISurveyAnswer } from '../models/surveyAnswer';

const httpInstance = axios.create({
  baseURL: '/api/survey-answers',
});

httpInstance.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error && error.response && error.response.data) {
      return Promise.reject(
        error.response.data.error
          ? error.response.data.error
          : error.response.data,
      );
    } else {
      return Promise.reject({ message: 'Server error' });
    }
  },
);

export enum SURVEY_TYPE {
  SURVEY,
  QUIZ,
}

const cleanupSurveyAnswerBody = (
  surveyAnswer: ISurveyAnswer,
): ISurveyAnswer => {
  delete surveyAnswer._id;
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  delete surveyAnswer.id;
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  delete surveyAnswer['__v'];
  return surveyAnswer;
};

export const getUserSurveyAnswerById = async (
  id: string,
): Promise<ISurveyAnswer[]> => {
  return await httpInstance.get(`/${id}/my-answers`);
};

export const createOrUpdateSurveyAnswer = async (
  surveyAnswer: ISurveyAnswer,
): Promise<ISurveyAnswer> => {
  const isUpdate = !!surveyAnswer._id;
  const url = isUpdate ? `/${surveyAnswer._id}` : ``;
  const req = isUpdate ? httpInstance.patch : httpInstance.post;
  return await req(url, cleanupSurveyAnswerBody(surveyAnswer));
};
