import axios from 'axios';
import { ICmsSection } from '../models/cmsSection';

const httpInstance = axios.create({
  baseURL: '/api/cms',
});

httpInstance.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error && error.response && error.response.data) {
      return Promise.reject(
        error.response.data.error ? error.response.data.error : error.response.data,
      );
    } else {
      return Promise.reject({ message: 'Server error' });
    }
  },
);

const cleanupCmsSectionBody = (cmsSection: Partial<ICmsSection>): Partial<ICmsSection> => {
  delete cmsSection._id;
  // @ts-ignore
  delete cmsSection.id;
  // @ts-ignore
  delete cmsSection['__v'];
  // @ts-ignore
  delete cmsSection.files;
  // @ts-ignore
  delete cmsSection.uploadFiles;

  return cmsSection;
};

export const uploadCmsSectionFiles = async (
  cmsSectionId: string,
  files: any[],
  accessToken: string,
) => {
  const formData = new FormData();

  for (let i = 0; i < files.length; i++) {
    formData.append('files', files[i]);
  }

  await axios.post(`${process.env.NEXT_PUBLIC_API_URL}/api/cms/${cmsSectionId}/upload`, formData, {
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
};

export const createOrUpdateCmsSection = async (
  cmsSection: Partial<ICmsSection>,
): Promise<ICmsSection> => {
  const isUpdate = !!cmsSection._id;
  const url = isUpdate ? `/${cmsSection._id}` : ``;
  const req = isUpdate ? httpInstance.patch : httpInstance.post;

  // @ts-ignore
  const { uploadFiles, accessToken, ...data } = cmsSection;
  const response = await req(`${url}`, cleanupCmsSectionBody(data));

  if (uploadFiles?.length) {
    // @ts-ignore
    await uploadCmsSectionFiles(response?._id, uploadFiles, accessToken);
  }

  // @ts-ignore
  return response;
};

export const deleteCmsSection = async (id: string): Promise<any> => {
  return await httpInstance.delete(`/${id}`);
};

export const getCMSSections = async (params: any): Promise<any> => {
  return await httpInstance.get(`/`, { params });
};
