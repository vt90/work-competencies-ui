import axios, { AxiosInstance } from 'axios';
import { IQuestion } from '../models/question';

const httpInstance = axios.create({
  baseURL: '/api/questions',
});

httpInstance.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error && error.response && error.response.data) {
      return Promise.reject(
        error.response.data.error ? error.response.data.error : error.response.data,
      );
    } else {
      return Promise.reject({ message: 'Server error' });
    }
  },
);

export enum QUESTION_TYPE {
  RADIO_SELECT,
  SHORT_ANSWER,
  LONG_ANSWER,
  MULTIPLE_SELECT,
}

const cleanupQuestionBody = (question: IQuestion): IQuestion => {
  if (
    question.type !== QUESTION_TYPE.RADIO_SELECT &&
    question.type !== QUESTION_TYPE.MULTIPLE_SELECT
  ) {
    delete question.answers;
  }

  if (question.answers) {
    question.answers.forEach((answer) => {
      if (!answer._id) delete answer._id;
    });
  }

  return question;
};

export const getSurveyQuestions = async (surveyId: string): Promise<IQuestion[]> => {
  return await httpInstance.get('', { params: { surveyId } });
};

export const createOrUpdateQuestion = async (question: IQuestion): Promise<IQuestion> => {
  const isUpdate = !!question._id;
  const url = isUpdate ? `/${question._id}` : ``;
  const req = isUpdate ? httpInstance.patch : httpInstance.post;
  return await req(url, cleanupQuestionBody(question));
};

export const deleteQuestion = async (questionId: string): Promise<any> => {
  return await httpInstance.delete(`/${questionId}`);
};
