import axios from 'axios';
import { IOrganization, IOrganizationUser } from '../models/organization';
import { IUser } from '../models/user';

const TENANT = process.env.NEXT_PUBLIC_TENANT;

const httpInstance = axios.create({
  baseURL: '/api/organizations',
});

httpInstance.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error && error.response && error.response.data) {
      return Promise.reject(
        error.response.data.error ? error.response.data.error : error.response.data,
      );
    } else {
      return Promise.reject({ message: 'Server error' });
    }
  },
);

export const getOrganizations = async (): Promise<IOrganization[]> => {
  return await httpInstance.get('', { params: { tenant: TENANT } });
};

export const getOrganizationById = async (id: string): Promise<IOrganization> => {
  return await httpInstance.get(`/${id}`);
};

export const getOrganizationUsersByOrganizationId = async (id: string): Promise<IUser[]> => {
  return await httpInstance.get(`/${id}/users`);
};

export const getOrganizationUsersByOrganizationName = async (name: string): Promise<IUser[]> => {
  return await httpInstance.get(`/${name}/organization-users`, { params: { tenant: TENANT } });
};

export const createOrganization = async (organization: IOrganization): Promise<IOrganization> => {
  const url = organization?._id ? `/${organization?._id}` : '';
  const req = organization?._id ? httpInstance.patch : httpInstance.post;
  return await req(url, { ...organization, tenant: TENANT });
};

export const inviteUserToOrganization = async (
  organizationUser: Partial<IOrganizationUser>,
): Promise<any> => {
  const orgId = organizationUser.organizationId;

  delete organizationUser.organizationId;
  delete organizationUser.isAdmin;
  organizationUser.tenant = TENANT;

  return await httpInstance.post(`${orgId}/users/invite`, organizationUser);
};

export const inviteUserToOrganizationByName = async (
  organizationUser: Partial<IOrganizationUser>,
): Promise<any> => {
  const orgId = organizationUser.organizationId;

  delete organizationUser.organizationId;
  delete organizationUser.isAdmin;
  organizationUser.tenant = TENANT;

  return await httpInstance.post(`${orgId}/organization-users/invite`, organizationUser);
};

export const removeUserFromOrganization = async (userId: string, orgId: string): Promise<any> => {
  return await httpInstance.delete(`${orgId}/organization-users/${userId}`, {
    params: { tenant: TENANT },
  });
};

export const removeOrganization = async (orgId: string): Promise<any> => {
  return await httpInstance.delete(`${orgId}`, {
    params: { tenant: TENANT },
  });
};
