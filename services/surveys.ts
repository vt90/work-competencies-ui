import axios, { AxiosInstance } from 'axios';
import { ISurvey } from '../models/survey';

const httpInstance = axios.create({
  baseURL: '/api/surveys',
});

httpInstance.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error && error.response && error.response.data) {
      return Promise.reject(
        error.response.data.error ? error.response.data.error : error.response.data,
      );
    } else {
      return Promise.reject({ message: 'Server error' });
    }
  },
);

export enum SURVEY_TYPE {
  SURVEY,
  QUIZ,
}

const cleanupSurveyBody = (survey: ISurvey): ISurvey => {
  delete survey._id;
  // @ts-ignore
  delete survey.id;
  // @ts-ignore
  delete survey['__v'];
  delete survey.type;
  delete survey.questions;
  delete survey.surveyAnswers;

  return survey;
};

export const getSurveys = async (params: ISurvey): Promise<ISurvey[]> => {
  return await httpInstance.get('', {
    params: { ...params, type: SURVEY_TYPE.SURVEY },
  });
};

export const getSurveysManagement = async (params: ISurvey): Promise<ISurvey[]> => {
  return await httpInstance.get('/management/find', {
    params: { ...params, type: `${SURVEY_TYPE.SURVEY}` },
  });
};

export const getSurveyById = async (id: string): Promise<ISurvey> => {
  return await httpInstance.get(`/${id}`);
};

export const getSurveyByIdManagement = async (id: string): Promise<ISurvey> => {
  return await httpInstance.get(`/management/${id}`);
};

export const createOrUpdateSurvey = async (survey: ISurvey): Promise<ISurvey> => {
  const isUpdate = !!survey._id;
  const url = isUpdate ? `/${survey._id}` : ``;
  const req = isUpdate ? httpInstance.patch : httpInstance.post;
  return await req(url, cleanupSurveyBody(survey));
};

export const createOrUpdateQuiz = async (survey: ISurvey): Promise<ISurvey> => {
  const isUpdate = !!survey._id;
  const url = isUpdate ? `/quiz/${survey._id}` : `/quiz`;
  const req = isUpdate ? httpInstance.patch : httpInstance.post;
  return await req(url, cleanupSurveyBody(survey));
};

export const deleteSurvey = async (id: string): Promise<any> => {
  return await httpInstance.delete(`/management/${id}`);
};


export const validateQuizAnswers = async (id: string): Promise<any> => {
  return await httpInstance.get(`/validate-quiz-answers/${id}`);
};
