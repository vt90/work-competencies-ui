import axios, { AxiosInstance } from 'axios';
import { ICourseMaterial } from '../models/course';

const httpInstance = axios.create({
  baseURL: '/api/course-materials',
});

httpInstance.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error && error.response && error.response.data) {
      return Promise.reject(
        error.response.data.error ? error.response.data.error : error.response.data,
      );
    } else {
      return Promise.reject({ message: 'Server error' });
    }
  },
);

const cleanupCourseMaterialBody = (
  courseMaterial: Partial<ICourseMaterial>,
): Partial<ICourseMaterial> => {
  delete courseMaterial._id;
  // @ts-ignore
  delete courseMaterial.id;
  // @ts-ignore
  delete courseMaterial['__v'];
  // @ts-ignore
  delete courseMaterial.files;
  // @ts-ignore
  delete courseMaterial.uploadFiles;

  return courseMaterial;
};

export const uploadCourseMaterialFiles = async (
  courseMaterialId: string,
  files: any[],
  accessToken: string,
) => {
  const formData = new FormData();

  for (let i = 0; i < files.length; i++) {
    formData.append('files', files[i]);
  }

  await axios.post(
    `${process.env.NEXT_PUBLIC_API_URL}/api/course-materials/${courseMaterialId}/upload`,
    formData,
    {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    },
  );
};

export const createOrUpdateCourseMaterial = async (
  courseMaterial: Partial<ICourseMaterial>,
): Promise<ICourseMaterial> => {
  const isUpdate = !!courseMaterial._id;
  const url = isUpdate ? `/${courseMaterial._id}` : ``;
  const req = isUpdate ? httpInstance.patch : httpInstance.post;

  // @ts-ignore
  const { uploadFiles, accessToken, ...data } = courseMaterial;
  const response = await req(`${url}`, cleanupCourseMaterialBody(data));

  if (uploadFiles?.length) {
    // @ts-ignore
    await uploadCourseMaterialFiles(response?._id, uploadFiles, accessToken);
  }

  // @ts-ignore
  return response;
};

export const deleteCourseMaterial = async (id: string): Promise<any> => {
  return await httpInstance.delete(`/${id}`);
};
