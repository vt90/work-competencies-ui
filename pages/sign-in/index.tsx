import React from 'react';
import { getCsrfToken, getProviders } from 'next-auth/react';
import { NextPageContext } from 'next';
import LoginForm from '../../components/Auth/LoginForm';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';

interface ISignInProps {
  csrfToken: string;
  providers: any[];
}

const SignIn = (props: ISignInProps) => {
  const { csrfToken } = props;

  return (
    <Container maxWidth="md">
      <Grid container alignItems="center" justifyContent="center" sx={{ minHeight: '100vh' }}>
        <Grid item xs={10} sm={5}>
          <LoginForm csrfToken={csrfToken} />
        </Grid>
      </Grid>
    </Container>
  );
};

export const getServerSideProps = async (context: NextPageContext) => {
  return {
    props: {
      providers: await getProviders(),
      csrfToken: await getCsrfToken(context),
    },
  };
};

export default SignIn;
