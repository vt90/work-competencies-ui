import Error from 'next/error';
import Box from '@mui/material/Box';

export default function Page() {
  return (
    <Box
      sx={{
        '& > div': {
          border: '1px solid white',
          maxHeight: 'calc(100vh - 180px)',
        },
      }}
    >
      <Error statusCode={404} title="pagina invalida" />
    </Box>
  );
}
