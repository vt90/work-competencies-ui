import React, { useCallback, useEffect, useMemo, useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { withPageAuthRequired } from '../../../../guards/authRequired';
import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Collapse from '@mui/material/Collapse';
import Grid from '@mui/material/Grid';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';

import AdminPage from '../../../../components/AdminPage';
import ListLoadingIndicator from '../../../../components/Common/ListLoadingIndcator';
import QuestionsForm from '../../../../components/Questions/Form';
import QuestionsList from '../../../../components/Questions/List';
import { useAuthContext } from '../../../../context/authContext';
import { IQuestion } from '../../../../models/question';
import {
  createOrUpdateSurvey,
  getSurveyByIdManagement,
  SURVEY_TYPE,
} from '../../../../services/surveys';
import {
  createOrUpdateQuestion,
  deleteQuestion,
  QUESTION_TYPE,
} from '../../../../services/questions';
import { SURVEY_VIEWS } from '../../../../models/survey';
import SurveyStats from '../../../../components/Surveys/Stats';
import { ISurveyAnswer } from '../../../../models/surveyAnswer';
import SurveyIndividualFilter from '../../../../components/Surveys/IndividualFilter';
import SurveyPublishedAlert from '../../../../components/Surveys/PublishedAlert';
import BaseModelShare from '../../../../components/Common/BaseModelShare';
import NoDataAvailable from '../../../../components/Common/NoDataAvailable';
import TextField from '@mui/material/TextField';
import { useDebouncedCallback } from 'use-debounce';

const Surveys = () => {
  const queryClient = useQueryClient();
  const { organization } = useAuthContext();
  const router = useRouter();
  const [selectedQuestion, setSelectedQuestion] = useState<IQuestion | null>(null);
  const { id, activeView, individual } = router.query;
  const FETCH_QUERY_KEY = useMemo(() => ['surveys', id], [id]);

  const { isLoading: isSurveyLoading, data: survey } = useQuery(
    FETCH_QUERY_KEY,
    () => getSurveyByIdManagement(id as string),
    {
      onError: () => router.push('/404'),
    },
  );

  const { isLoading: isQuestionsMutating, mutate: createOrUpdateQuestionMutation } = useMutation(
    createOrUpdateQuestion,
    {
      onSuccess: () => revalidateSurveyQuestions(),
    },
  );

  const { mutate: deleteQuestionMutation } = useMutation(deleteQuestion, {
    onSuccess: () => revalidateSurveyQuestions(),
  });

  const { isLoading: isSurveyMutating, mutate: updateSurveyMutation } = useMutation(
    createOrUpdateSurvey,
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries(FETCH_QUERY_KEY);
      },
    },
  );

  const revalidateSurveyQuestions = useCallback(async () => {
    await queryClient.invalidateQueries(FETCH_QUERY_KEY);
    setSelectedQuestion(null);
  }, [FETCH_QUERY_KEY, queryClient]);

  const onSubmit = (question: IQuestion) => {
    createOrUpdateQuestionMutation({
      ...selectedQuestion,
      ...question,
      surveyId: `${id}`,
      orgId: organization,
    });
  };

  const onUpdateSurveyPublishedFlag = (value: boolean) => {
    updateSurveyMutation({
      ...survey,
      published: value,
    });
  };

  const debouncedChange = useDebouncedCallback(
    (name, value) =>
      updateSurveyMutation({
        ...survey,
        [name]: value,
      }),
    1000,
  );

  const onDelete = (question: IQuestion) => {
    question._id && deleteQuestionMutation(question._id);
  };

  const handleViewChange = useCallback(
    (activeView: string) =>
      router.push({
        pathname: '/management/surveys/[id]',
        query: { id, activeView },
      }),
    [id, router],
  );

  const handleIndividualChange = (individual: string | undefined) => {
    router.push({
      pathname: '/management/surveys/[id]',
      query: {
        id,
        individual,
        activeView: SURVEY_VIEWS.INDIVIDUALS,
      },
    });
  };

  useEffect(() => {
    const existingView = activeView && Object.values(SURVEY_VIEWS).find((v) => v === activeView);

    if (!existingView) handleViewChange(SURVEY_VIEWS.STRUCT);
  }, [activeView, handleViewChange]);

  if (isSurveyLoading) return <ListLoadingIndicator />;

  // ToDo implement not found
  if (!survey) return 'Survey unavailable';

  return (
    <>
      <Head>
        <title>Chestionare</title>
      </Head>

      <AdminPage
        actions={
          activeView === SURVEY_VIEWS.STRUCT && (
            <>
              <Button
                onClick={() =>
                  setSelectedQuestion({
                    type: QUESTION_TYPE.RADIO_SELECT,
                    answers: [{ text: 'Opțiunea 1' }],
                  })
                }
                variant="contained"
              >
                Adaugă Întrebare
              </Button>
            </>
          )
        }
        title={
          <Grid container spacing={2} alignItems="center">
            <Grid item xs={12} md={true}>
              <TextField
                defaultValue={survey.name}
                onChange={(ev) => debouncedChange('name', ev.target.value)}
                multiline
                fullWidth
                sx={{
                  '& legend': { display: 'none' },
                  '& fieldset': {
                    top: 0,
                    borderColor: 'transparent',
                  },
                  '& .MuiInputBase-root': { py: 0.5, pl: 0.25 },
                }}
                inputProps={{
                  sx: {
                    typography: 'h5',
                    fontWeight: 'bold',
                  },
                }}
              />
            </Grid>
            <Grid item md={false} sx={{ display: 'flex', alignItems: 'center' }}>
              <Box mr={1}>
                <SurveyPublishedAlert
                  survey={survey}
                  isLoading={isSurveyLoading || isSurveyMutating}
                  onChange={onUpdateSurveyPublishedFlag}
                />
              </Box>
              <Collapse orientation="horizontal" in={survey.published}>
                <BaseModelShare model={survey} endpoint="surveys" />
              </Collapse>
            </Grid>
          </Grid>
        }
        pages={[
          { url: '/management/surveys', name: 'Chestionare' },
          { url: `/management/surveys/${id}`, name: `${survey.name}` },
        ]}
      >
        {survey?.questions?.length ? (
          <>
            <Box mb={3}>
              <TextField
                defaultValue={survey.description}
                onChange={(ev) => debouncedChange('description', ev.target.value)}
                multiline
                fullWidth
                sx={{
                  '& legend': { display: 'none' },
                  '& fieldset': {
                    top: 0,
                    borderColor: 'transparent',
                  },
                  '& .MuiInputBase-root': { py: 0.5, pl: 0.25 },
                }}
                inputProps={{
                  sx: {
                    typography: 'body1',
                    fontWeight: 'bold',
                  },
                }}
              />
            </Box>

            <Box sx={{ borderBottom: 1, borderColor: 'divider', my: 4 }}>
              <Tabs value={activeView} onChange={(_, newValue) => handleViewChange(newValue)}>
                {Object.values(SURVEY_VIEWS).map((view) => (
                  <Tab key={view} value={view} label={view} />
                ))}
              </Tabs>
            </Box>

            {activeView === SURVEY_VIEWS.RESULTS && <SurveyStats survey={survey} />}

            {activeView === SURVEY_VIEWS.INDIVIDUALS && (
              <SurveyIndividualFilter
                survey={survey}
                onSelectAnswerUser={(answer: ISurveyAnswer) =>
                  handleIndividualChange(answer.userId)
                }
                // @ts-ignore
                selectedIndividual={individual}
              />
            )}

            <QuestionsList
              readonly={false}
              onDeleteQuestion={onDelete}
              onEditQuestion={setSelectedQuestion}
              onSelectAnswerUser={(answer: ISurveyAnswer) => handleIndividualChange(answer.userId)}
              // @ts-ignore
              view={activeView}
              // @ts-ignore
              questions={survey?.questions}
              // @ts-ignore
              survey={survey}
              // @ts-ignore
              individual={individual}
            />
          </>
        ) : (
          <NoDataAvailable
            content={`În momentul de faţă chestionarul ${survey?.name} nu conţine nicio întrebare`}
          />
        )}

        {selectedQuestion && (
          <QuestionsForm
            open={!!selectedQuestion}
            onClose={() => setSelectedQuestion(null)}
            onSubmit={onSubmit}
            isLoading={isQuestionsMutating}
            isQuiz={selectedQuestion?.type === SURVEY_TYPE.QUIZ}
            initialValues={selectedQuestion}
          />
        )}
      </AdminPage>
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Surveys;
