import React, { useState } from 'react';
import Head from 'next/head';
import { withPageAuthRequired } from '../../../guards/authRequired';
import {
  createOrUpdateSurvey,
  deleteSurvey,
  getSurveysManagement,
  SURVEY_TYPE,
} from '../../../services/surveys';
import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';
import Button from '@mui/material/Button';
import SurveysForm from '../../../components/Surveys/Form';
import AdminPage from '../../../components/AdminPage';
import SurveysList from '../../../components/Surveys/List';
import ListLoadingIndicator from '../../../components/Common/ListLoadingIndcator';
import { ISurvey } from '../../../models/survey';
import { useAuthContext } from '../../../context/authContext';
import LoadingButton from '@mui/lab/LoadingButton';
import Typography from '@mui/material/Typography';
import Dialog from '../../../components/Common/Dialog';
import NoDataAvailable from '../../../components/Common/NoDataAvailable';

const FETCH_QUERY_KEY = ['surveys'];

const Surveys = () => {
  const [selectedSurvey, setSelectedSurvey] = useState<ISurvey | null>(null);
  const [deletingSurvey, setDeletingSurvey] = useState<ISurvey | null>(null);
  const { organization } = useAuthContext();

  const queryClient = useQueryClient();

  const { isLoading, data: surveys } = useQuery(
    FETCH_QUERY_KEY,
    () => getSurveysManagement({ orgId: organization }),
    {
      enabled: !!organization,
    },
  );

  const { isLoading: isMutating, mutate } = useMutation(createOrUpdateSurvey, {
    onSuccess: async () => {
      await queryClient.invalidateQueries(FETCH_QUERY_KEY);
      setSelectedSurvey(null);
    },
  });

  const { isLoading: isDeleting, mutate: deleteSurveyById } = useMutation(deleteSurvey, {
    onSuccess: async () => {
      await queryClient.invalidateQueries(FETCH_QUERY_KEY);
      setDeletingSurvey(null);
    },
  });

  const onSubmit = (survey: ISurvey) => {
    mutate({
      ...selectedSurvey,
      ...survey,
      orgId: organization,
    });
  };

  return (
    <>
      <Head>
        <title>Chestionare</title>
      </Head>

      <AdminPage
        actions={
          <>
            <Button
              onClick={() => setSelectedSurvey({ type: SURVEY_TYPE.SURVEY })}
              variant="contained"
            >
              Adaugă Chestionar
            </Button>
          </>
        }
        title="Chestionare"
        pages={[{ url: '/management/surveys', name: 'Chestionare' }]}
      >
        {surveys?.length || isLoading ? (
          <>
            {isLoading && <ListLoadingIndicator />}

            {surveys && (
              <SurveysList
                baseUrl="/management/surveys"
                surveys={surveys}
                // onSelectSurvey={setSelectedSurvey}
                onDeleteSurvey={setDeletingSurvey}
              />
            )}
          </>
        ) : (
          <NoDataAvailable content="În momentul de faţă nu există niciun chestionar disponibil pe platformă" />
        )}

        {selectedSurvey && (
          <SurveysForm
            open={!!selectedSurvey}
            onClose={() => setSelectedSurvey(null)}
            onSubmit={onSubmit}
            isLoading={isMutating}
            initialValues={selectedSurvey}
          />
        )}

        <Dialog
          actions={
            <LoadingButton
              variant="contained"
              onClick={() => deletingSurvey && deleteSurveyById(`${deletingSurvey?._id}`)}
              loading={isDeleting}
            >
              Șterge
            </LoadingButton>
          }
          content={
            <>
              <Typography color="textSecondary" textAlign="center">
                Sunteți siguri că doriți să ștergeți chestionarul
                <strong>
                  <i>&quot;{deletingSurvey?.name}&quot;</i>
                </strong>
                ?
              </Typography>
            </>
          }
          title="Confirmati ștergerea?"
          onClose={() => setDeletingSurvey(null)}
          open={!!deletingSurvey}
        />
      </AdminPage>
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Surveys;
