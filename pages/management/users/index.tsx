import React, { useState, useMemo } from 'react';
import Head from 'next/head';
import { withPageAuthRequired } from '../../../guards/authRequired';
import {
  getOrganizationUsersByOrganizationName,
  inviteUserToOrganizationByName,
  removeUserFromOrganization,
} from '../../../services/organizations';

import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';
import Box from '@mui/material/Box';
import AdminPage from '../../../components/AdminPage';
import ListLoadingIndicator from '../../../components/Common/ListLoadingIndcator';
import { IOrganizationUser } from '../../../models/organization';
import UserInvitationForm from '../../../components/Users/InvitationForm';
import Card from '@mui/material/Card';
import { ORG_ADMIN_PREFIX, ORG_USER_PREFIX, IUser } from '../../../models/user';
import { useAuthContext } from '../../../context/authContext';
import UsersList from '../../../components/Users/List';
import Typography from '@mui/material/Typography';
import Dialog from '../../../components/Common/Dialog';
import LoadingButton from '@mui/lab/LoadingButton';

const Organizations = () => {
  const [deletingUser, setDeletingUser] = useState<IUser | null>(null);
  const { organization } = useAuthContext();
  const queryClient = useQueryClient();

  const FETCH_ADMINS_QUERY_KEY = useMemo(
    () => ['organization-admin-users', organization],
    [organization],
  );
  const FETCH_USERS_QUERY_KEY = useMemo(
    () => ['organization-user-users', organization],
    [organization],
  );

  const usersOrganization = useMemo(
    () => organization?.replace(ORG_ADMIN_PREFIX, ORG_USER_PREFIX),
    [organization],
  );

  const { data: organizationAdminUsers } = useQuery(
    FETCH_ADMINS_QUERY_KEY,
    () => getOrganizationUsersByOrganizationName(organization),
    {
      enabled: !!organization,
    },
  );

  const { isLoading, data: organizationUsers } = useQuery(
    FETCH_USERS_QUERY_KEY,
    () => getOrganizationUsersByOrganizationName(usersOrganization),
    {
      enabled: !!usersOrganization,
    },
  );

  const { isLoading: isInviting, mutate: invite } = useMutation(inviteUserToOrganizationByName, {
    onSuccess: () => {
      organization && queryClient.invalidateQueries(FETCH_ADMINS_QUERY_KEY);
      queryClient.invalidateQueries(FETCH_USERS_QUERY_KEY);
    },
  });

  const { isLoading: isDeleting, mutate: deleteUserByOrgAndId } = useMutation(
    // @ts-ignore
    (user: IUser) => removeUserFromOrganization(user._id, user.userRoles[0].name),
    {
      onSuccess: () => {
        organization && queryClient.invalidateQueries(FETCH_ADMINS_QUERY_KEY);
        queryClient.invalidateQueries(FETCH_USERS_QUERY_KEY);
        setDeletingUser(null);
      },
    },
  );

  const onInvite = (data: IOrganizationUser) => {
    const role = data.isAdmin
      ? organization
      : organization.replace(ORG_ADMIN_PREFIX, ORG_USER_PREFIX);

    organization &&
      invite({
        ...data,
        organizationId: role,
        role,
      });
  };

  const onDeleteAdmin = (user: IUser) => {
    setDeletingUser({
      ...user,
      userRoles: [{ _id: '', name: organization }],
    });
  };

  const onDeleteUser = (user: IUser) => {
    setDeletingUser({
      ...user,
      userRoles: [{ _id: '', name: organization.replace(ORG_ADMIN_PREFIX, ORG_USER_PREFIX) }],
    });
  };

  return (
    <>
      <Head>
        <title>Utilizatori</title>
      </Head>

      <AdminPage pages={[{ url: '/management/users', name: 'Utilizatori' }]} title="Utilizatori">
        <Card sx={{ py: 2, px: 3, mb: 3 }}>
          {isLoading ? (
            <ListLoadingIndicator />
          ) : (
            <UserInvitationForm showOrgSwitcher={true} isLoading={isInviting} onSubmit={onInvite} />
          )}
        </Card>

        <Card>
          <Box px={2} py={3}>
            {isLoading ? (
              <ListLoadingIndicator />
            ) : (
              <>
                {organizationAdminUsers?.length ? (
                  <Box mb={2}>
                    <Typography gutterBottom>Administratori</Typography>
                    <UsersList users={organizationAdminUsers} onDeleteUser={onDeleteAdmin} />
                  </Box>
                ) : null}

                {organizationUsers?.length ? (
                  <Box>
                    <Typography gutterBottom>Utilizatori</Typography>
                    <UsersList users={organizationUsers} onDeleteUser={onDeleteUser} />
                  </Box>
                ) : null}
              </>
            )}
          </Box>
        </Card>

        <Dialog
          actions={
            <LoadingButton
              variant="contained"
              onClick={() => deletingUser && deleteUserByOrgAndId(deletingUser)}
              loading={isDeleting}
            >
              Șterge
            </LoadingButton>
          }
          content={
            <>
              <Typography color="textSecondary" textAlign="center">
                Sunteți siguri că doriți să ștergeți utilizatorul
                <strong>
                  <i>&quot;{deletingUser?.name}&quot;</i>
                </strong>
                ?
              </Typography>
            </>
          }
          title="Confirmati ștergerea?"
          onClose={() => setDeletingUser(null)}
          open={!!deletingUser}
        />
      </AdminPage>
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Organizations;
