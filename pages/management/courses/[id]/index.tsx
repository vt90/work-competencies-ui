import React, { useMemo, useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useDebouncedCallback } from 'use-debounce';
import { withPageAuthRequired } from '../../../../guards/authRequired';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { useAuthContext } from '../../../../context/authContext';
import { getCourseByIdManagement, createOrUpdateCourse } from '../../../../services/courses';
import ListLoadingIndicator from '../../../../components/Common/ListLoadingIndcator';
import AdminPage from '../../../../components/AdminPage';
import dynamic from 'next/dynamic';
import CoursePublishedAlert from '../../../../components/Courses/PublishedAlert';
import BaseModelShare from '../../../../components/Common/BaseModelShare';
import Collapse from '@mui/material/Collapse';
import CourseReviews from '../../../../components/Courses/Reviews';
import Tabs from '@mui/material/Tabs';
import { COURSE_VIEWS, ICourseMaterial } from '../../../../models/course';
import Tab from '@mui/material/Tab';
import CourseTest from '../../../../components/Courses/CourseTest';
import { ISurvey } from '../../../../models/survey';
import {
  createOrUpdateQuiz,
  deleteSurvey,
  getSurveyByIdManagement,
} from '../../../../services/surveys';
import Button from '@mui/lab/LoadingButton';
import CourseMaterialForm from '../../../../components/Courses/CourseMaterialForm';
import {
  createOrUpdateCourseMaterial,
  deleteCourseMaterial,
} from '../../../../services/courseMaterials';
import CourseMaterialList from '../../../../components/Courses/CourseMaterialList';
import LoadingButton from '@mui/lab/LoadingButton';
import Typography from '@mui/material/Typography';
import Dialog from '../../../../components/Common/Dialog';
const RichText = dynamic(() => import('../../../../components/Common/RichText'), {
  ssr: false,
});

const Course = () => {
  const [activeView, setActiveView] = useState(COURSE_VIEWS.COURSE);
  const [courseMaterial, setCourseMaterial] = useState<Partial<ICourseMaterial> | null>(null);
  const [courseMaterialToDelete, setCourseMaterialToDelete] =
    useState<Partial<ICourseMaterial> | null>(null);
  const router = useRouter();
  const { accessToken, organization } = useAuthContext();
  const queryClient = useQueryClient();

  const { id } = router.query;
  const FETCH_QUERY_KEY = useMemo(() => ['courses', id], [id]);
  const FETCH_QUERY_SURVEY_KEY = useMemo(() => ['coursesSurvey', id], [id]);

  const {
    isLoading,
    data: course,
    isRefetching,
  } = useQuery(FETCH_QUERY_KEY, () => getCourseByIdManagement(id as string), {
    enabled: !!(organization && id),
    onError: () => router.push('/404'),
  });

  const { data: courseSurvey } = useQuery(
    FETCH_QUERY_SURVEY_KEY,
    () => getSurveyByIdManagement(course?.surveyId as string),
    {
      enabled: !!(organization && course && course.surveyId),
      onError: () => router.push('/404'),
    },
  );

  const { isLoading: isUpdating, mutate } = useMutation(createOrUpdateCourse, {
    onSuccess: async () => {
      await queryClient.invalidateQueries(FETCH_QUERY_KEY);
    },
  });

  const { isLoading: isSurveyLoading, mutate: createQuiz } = useMutation(createOrUpdateQuiz, {
    onSuccess: async (data) => {
      mutate({
        ...course,
        // @ts-ignore
        surveyId: data._id,
      });
    },
  });

  const { isLoading: isCourseMaterialLoading, mutate: manageCourseMaterial } = useMutation(
    createOrUpdateCourseMaterial,
    {
      onSuccess: async () => {
        setCourseMaterial(null);
        setCourseMaterialToDelete(null);
        await queryClient.invalidateQueries(FETCH_QUERY_KEY);
      },
    },
  );

  const debouncedChange = useDebouncedCallback(
    (name, value) =>
      mutate({
        ...course,
        [name]: value,
      }),
    1000,
  );

  const onPublishedChanged = (value: boolean) => {
    mutate({
      ...course,
      published: value,
    });
  };

  const onCreateSurvey = () => {
    const quiz: Partial<ISurvey> = {
      name: `Chestionar de evaluare ${course?.title}`,
      description: '',
      published: false,
      orgId: organization,
    };

    createQuiz(quiz);
  };

  const { mutate: deleteSurveyById } = useMutation(deleteSurvey, {
    onSuccess: async () => {
      mutate({
        ...course,
        // @ts-ignore
        surveyId: null,
      });
    },
  });

  const { isLoading: isDeleting, mutate: deleteCourseMaterialById } = useMutation(
    deleteCourseMaterial,
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries(FETCH_QUERY_KEY);
        setCourseMaterialToDelete(null);
      },
    },
  );

  if (isLoading) return <ListLoadingIndicator />;

  return (
    <div>
      <Head>
        <title>Curs</title>
      </Head>

      {course ? (
        <AdminPage
          actions={
            <>
              <Collapse
                in={activeView === COURSE_VIEWS.DOCUMENTS}
                orientation="horizontal"
                sx={{ maxHeight: '36.5px' }}
              >
                <Box>
                  <Button
                    // @ts-ignore
                    onClick={() => setCourseMaterial({})}
                    variant="contained"
                    sx={{ mr: 1 }}
                  >
                    Adaugă Material Curs
                  </Button>
                </Box>
              </Collapse>
            </>
          }
          pages={[
            { url: '/management/courses', name: 'Cursuri' },
            // @ts-ignore
            { url: `/management/courses/${id}`, name: course.title },
          ]}
          title={
            <Grid container spacing={2} alignItems="center">
              <Grid item xs={12} md={true}>
                <TextField
                  defaultValue={course.title}
                  onChange={(ev) => debouncedChange('title', ev.target.value)}
                  multiline
                  fullWidth
                  sx={{
                    '& legend': { display: 'none' },
                    '& fieldset': {
                      top: 0,
                      borderColor: 'transparent',
                    },
                    '& .MuiInputBase-root': { py: 0.5, pl: 0.25 },
                  }}
                  inputProps={{
                    sx: {
                      typography: 'h5',
                      fontWeight: 'bold',
                    },
                  }}
                />
              </Grid>
              <Grid item md={false} sx={{ display: 'flex', alignItems: 'center' }}>
                <Box mr={1}>
                  <CoursePublishedAlert
                    course={course}
                    isLoading={isUpdating}
                    onChange={onPublishedChanged}
                  />
                </Box>
                <Collapse orientation="horizontal" in={course.published}>
                  <BaseModelShare model={course} endpoint="courses" />
                </Collapse>
              </Grid>
            </Grid>
          }
        >
          <Box sx={{ borderBottom: 1, borderColor: 'divider', mb: 4 }}>
            <Tabs value={activeView} onChange={(_, newValue) => setActiveView(newValue)}>
              {Object.values(COURSE_VIEWS).map((view) => (
                <Tab key={view} value={view} label={view} />
              ))}
            </Tabs>
          </Box>

          {activeView === COURSE_VIEWS.COURSE && (
            <RichText
              defaultValue={course.content}
              onChange={(value) => debouncedChange('content', value)}
            />
          )}

          {activeView === COURSE_VIEWS.TESTS && (
            <CourseTest
              course={course}
              courseSurvey={courseSurvey}
              isLoading={isSurveyLoading}
              onCreateSurvey={onCreateSurvey}
              // @ts-ignore
              onDeleteSurvey={() => deleteSurveyById(course?.surveyId)}
            />
          )}
          {activeView === COURSE_VIEWS.REVIEWS && <CourseReviews course={course} />}

          {activeView === COURSE_VIEWS.DOCUMENTS &&
            (isRefetching ? (
              <ListLoadingIndicator />
            ) : (
              <CourseMaterialList
                course={course}
                onSelectCourseMaterial={(courseMaterial) => setCourseMaterial(courseMaterial)}
                onDeleteCourseMaterial={(courseMaterial) =>
                  setCourseMaterialToDelete(courseMaterial)
                }
              />
            ))}

          {courseMaterial && (
            <CourseMaterialForm
              initialValues={courseMaterial}
              isLoading={isCourseMaterialLoading}
              open={true}
              onClose={() => setCourseMaterial(null)}
              onSubmit={(data) => manageCourseMaterial({ ...data, accessToken, courseId: id })}
              // @ts-ignore
              onDeleteCourseMaterialFile={null}
            />
          )}

          <Dialog
            actions={
              <Box ml={1}>
                {courseMaterialToDelete && (
                  <LoadingButton
                    variant="contained"
                    onClick={() => deleteCourseMaterialById(`${courseMaterialToDelete?._id}`)}
                    loading={isDeleting}
                  >
                    Șterge
                  </LoadingButton>
                )}
              </Box>
            }
            content={
              <Typography color="textSecondary" textAlign="center">
                Sunteți siguri că doriți să ștergeți cursul
                <strong>
                  <i>
                    &quot;
                    {courseMaterialToDelete?.title}
                    &quot;
                  </i>
                </strong>
                ?
              </Typography>
            }
            title="Confirmati ștergerea?"
            onClose={() => setCourseMaterialToDelete(null)}
            open={!!courseMaterialToDelete}
          />
        </AdminPage>
      ) : null}
    </div>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Course;
