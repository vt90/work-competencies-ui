import React, { useState } from 'react';
import Head from 'next/head';
import { withPageAuthRequired } from '../../../guards/authRequired';
import { useAuthContext } from '../../../context/authContext';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import {
  checkExisting,
  createOrUpdateCourse,
  deleteCourse,
  getCoursesManagement,
} from '../../../services/courses';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import LoadingButton from '@mui/lab/LoadingButton';
import Typography from '@mui/material/Typography';
import AdminPage from '../../../components/AdminPage';
import ListLoadingIndicator from '../../../components/Common/ListLoadingIndcator';
import CoursesList from '../../../components/Courses/List';
import CourseCreateForm from '../../../components/Courses/CreateForm';
import { useRouter } from 'next/router';
import { ICourse } from '../../../models/course';
import Dialog from '../../../components/Common/Dialog';
import NoDataAvailable from '../../../components/Common/NoDataAvailable';

const FETCH_QUERY_KEY = ['courses'];

const Courses = () => {
  const [submittingCourse, setSubmittingCourse] = useState<Partial<ICourse> | null>(null);
  const [showExistingDialog, setShowExistingDialog] = useState(false);
  const [deletingCourseId, setShowDeletingCourseId] = useState<ICourse | null>(null);
  const [showCreateForm, setShowCreateForm] = useState(false);
  const { organization } = useAuthContext();
  const queryClient = useQueryClient();
  const router = useRouter();

  const { isLoading, data: courses } = useQuery(
    FETCH_QUERY_KEY,
    () => getCoursesManagement({ orgId: organization }),
    {
      enabled: !!organization,
    },
  );

  const { isLoading: isChecking, mutate: check } = useMutation(checkExisting, {
    onSuccess: async (data) => {
      if (data?.length) {
        setShowExistingDialog(true);
      } else {
        submittingCourse && create(submittingCourse);
      }
    },
  });

  const { isLoading: isDeleting, mutate: deleteCourseById } = useMutation(deleteCourse, {
    onSuccess: async () => {
      await queryClient.invalidateQueries(['courses']);
      setShowDeletingCourseId(null);
    },
  });

  const { isLoading: isCreating, mutate: create } = useMutation(createOrUpdateCourse, {
    onSuccess: async (data) => {
      const { _id } = data;
      await queryClient.invalidateQueries(['courses']);
      await router.push(`/management/courses/${_id}`);
    },
  });

  const onCreate = (values: Partial<ICourse>) => {
    values.orgId = organization;
    setSubmittingCourse(values);
    setTimeout(() => check(values), 300);
  };

  return (
    <div>
      <Head>
        <title>Cursuri</title>
      </Head>

      <AdminPage
        actions={
          <>
            <Button onClick={() => setShowCreateForm(true)} variant="contained">
              Adaugă Curs
            </Button>
          </>
        }
        title="Cursuri"
        pages={[{ url: '/management/courses', name: 'Cursuri' }]}
      >
        {courses?.length || isLoading ? (
          <>
            {isLoading && <ListLoadingIndicator />}

            {courses && (
              <CoursesList
                baseUrl="/management/courses"
                courses={courses}
                onDeleteCourse={(course) => setShowDeletingCourseId(course)}
                // onSelectCourse={(course) => router.push(`/management/courses/${course._id}`)}
              />
            )}
          </>
        ) : (
          <NoDataAvailable content="În momentul de faţă nu există niciun curs disponibil pe platformă" />
        )}

        <CourseCreateForm
          isLoading={isChecking || isCreating}
          open={showCreateForm}
          onClose={() => setShowCreateForm(false)}
          onSubmit={onCreate}
        />

        <Dialog
          actions={
            <Box ml={1}>
              {showExistingDialog && (
                <LoadingButton
                  variant="contained"
                  onClick={() => submittingCourse && create(submittingCourse)}
                  loading={isCreating}
                >
                  Crează
                </LoadingButton>
              )}

              {deletingCourseId && (
                <LoadingButton
                  variant="contained"
                  onClick={() => deletingCourseId && deleteCourseById(`${deletingCourseId?._id}`)}
                  loading={isDeleting}
                >
                  Șterge
                </LoadingButton>
              )}
            </Box>
          }
          content={
            <>
              <Typography color="textSecondary" textAlign="center">
                {showExistingDialog ? (
                  <>
                    Există deja cursul
                    <strong>
                      <i>&quot;{submittingCourse?.title}&quot;</i>
                    </strong>
                    ! Doriți să creați alt curs cu același nume?
                  </>
                ) : (
                  <>
                    Sunteți siguri că doriți să ștergeți cursul
                    <strong>
                      <i>&quot;{deletingCourseId?.title}&quot;</i>
                    </strong>
                    ?
                  </>
                )}
              </Typography>
            </>
          }
          title={showExistingDialog ? 'Curs existent!' : 'Confirmati ștergerea?'}
          onClose={() => {
            setShowExistingDialog(false);
            setShowDeletingCourseId(null);
          }}
          open={!!(showExistingDialog || deletingCourseId)}
        />
      </AdminPage>
    </div>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Courses;
