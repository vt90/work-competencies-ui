import React, { useState } from 'react';
import Head from 'next/head';
import { useSession } from 'next-auth/react';
import { withPageAuthRequired } from '../../../guards/authRequired';
import {
  createOrganization,
  getOrganizations,
  inviteUserToOrganization,
  removeOrganization,
} from '../../../services/organizations';

import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import OrganizationsForm from '../../../components/Organizations/Form';
import AdminPage from '../../../components/AdminPage';
import OrganizationsList from '../../../components/Organizations/List';
import ListLoadingIndicator from '../../../components/Common/ListLoadingIndcator';
import { IOrganization, IOrganizationUser } from '../../../models/organization';
import OrganizationDetails from '../../../components/Organizations/Details';
import SideDrawer from '../../../components/Common/SideDrawer';
import UserInvitationForm from '../../../components/Users/InvitationForm';
import Card from '@mui/material/Card';
import {
  isOrgAdmin,
  isOrgUser,
  isSuperAdmin,
  ORG_ADMIN_PREFIX,
  ORG_USER_PREFIX,
} from '../../../models/user';
import LoadingButton from '@mui/lab/LoadingButton';
import Typography from '@mui/material/Typography';
import Dialog from '../../../components/Common/Dialog';

const FETCH_QUERY_KEY = ['organizations'];

const Organizations = () => {
  const [selectedOrganization, setSelectedOrganization] = useState<IOrganization | null>(null);
  const [selectedOrganizationForDelete, setSelectedOrganizationForDelete] =
    useState<IOrganization | null>(null);
  const [showCreateForm, setShowCreateForm] = useState(false);
  const [createFormDefaultValues, setCreateFormDefaultValues] =
    // @ts-ignore
    useState<Partial<IOrganization>>(null);

  const queryClient = useQueryClient();
  const { update, data: session } = useSession();

  const { isLoading, data: organizations } = useQuery(FETCH_QUERY_KEY, getOrganizations);

  const { isLoading: isCreating, mutate: create } = useMutation(createOrganization, {
    onSuccess: (data) => {
      queryClient.invalidateQueries(['organizations']);
      setShowCreateForm(false);
      setSelectedOrganization(data);
      // @ts-ignore
      update({
        ...session,
        user: {
          // @ts-ignore
          ...session.user,
          userRoles: [
            // @ts-ignore
            ...session.user.userRoles,
            data,
          ],
        },
      });
    },
  });

  const { isLoading: isInviting, mutate: invite } = useMutation(inviteUserToOrganization, {
    onSuccess: () => {
      selectedOrganization &&
        queryClient.invalidateQueries(['organization-users', selectedOrganization._id]);
    },
  });

  const { isLoading: isDeleting, mutate: deleteOrg } = useMutation(removeOrganization, {
    onSuccess: () => {
      queryClient.invalidateQueries(['organizations']);
      // @ts-ignore
      const filteredRoles = session.user.userRoles.filter(
        // @ts-ignore
        (role) => {
          return role._id !== selectedOrganizationForDelete?._id;
        },
      );

      update({
        ...session,
        user: {
          // @ts-ignore
          ...session.user,
          userRoles: filteredRoles,
        },
      });

      setSelectedOrganizationForDelete(null);
    },
  });

  const onInvite = (data: IOrganizationUser) => {
    selectedOrganization &&
      invite({
        ...data,
        organizationId: selectedOrganization._id,
        role: selectedOrganization.name,
      });
  };

  const getSelectedOrganizationTitle = (role: string): string => {
    let displayRole = '';
    if (isSuperAdmin(role)) displayRole = 'Administratori Platforma';
    if (isOrgAdmin(role)) displayRole = `Administratori ${role.substring(ORG_ADMIN_PREFIX.length)}`;
    if (isOrgUser(role)) displayRole = `Utilizatori ${role.substring(ORG_USER_PREFIX.length)}`;
    return displayRole;
  };

  return (
    <>
      <Head>
        <title>Organizații</title>
      </Head>

      <AdminPage
        actions={
          <>
            <Button
              onClick={() => {
                setCreateFormDefaultValues({ name: '' });
                setShowCreateForm(true);
              }}
              variant="contained"
            >
              Adaugă Organizație
            </Button>
          </>
        }
        pages={[{ url: '/management/organizations', name: 'Organizații' }]}
        title="Organizații"
      >
        <Card>
          <Box px={2} py={3}>
            {isLoading && <ListLoadingIndicator />}

            {organizations && (
              <>
                <OrganizationsList
                  organizations={organizations}
                  onSelectOrganization={setSelectedOrganization}
                  onDeleteOrganization={setSelectedOrganizationForDelete}
                  onEditOrganization={(org) => {
                    setCreateFormDefaultValues(org);
                    setShowCreateForm(true);
                  }}
                />

                <SideDrawer
                  open={!!selectedOrganization}
                  onClose={() => setSelectedOrganization(null)}
                  title={getSelectedOrganizationTitle(`${selectedOrganization?.name}`)}
                  actions={<UserInvitationForm isLoading={isInviting} onSubmit={onInvite} />}
                >
                  {selectedOrganization && (
                    <Box
                      sx={{
                        minHeight: 'calc(100vh - 328px)',
                        maxHeight: 'calc(100vh - 328px)',
                        overflowY: 'scroll',
                      }}
                    >
                      <OrganizationDetails organisation={selectedOrganization} />
                    </Box>
                  )}
                </SideDrawer>
              </>
            )}
          </Box>
        </Card>

        {showCreateForm && (
          <OrganizationsForm
            isLoading={isCreating}
            open={showCreateForm}
            defaultValues={createFormDefaultValues}
            onClose={() => {
              // @ts-ignore
              setCreateFormDefaultValues(null);
              setShowCreateForm(false);
            }}
            onSubmit={create}
          />
        )}

        {selectedOrganizationForDelete && (
          <Dialog
            actions={
              <LoadingButton
                variant="contained"
                onClick={() =>
                  selectedOrganizationForDelete &&
                  deleteOrg(`${selectedOrganizationForDelete?.name}`)
                }
                loading={isDeleting}
              >
                Șterge
              </LoadingButton>
            }
            content={
              <>
                <Typography color="textSecondary" textAlign="center">
                  Sunteți siguri că doriți să ștergeți cursul
                  <strong>
                    <i>&quot;{selectedOrganizationForDelete?.name}&quot;</i>
                  </strong>
                  ?
                </Typography>
              </>
            }
            title="Confirmati ștergerea?"
            onClose={() => {
              setSelectedOrganizationForDelete(null);
            }}
            open={!!selectedOrganizationForDelete}
          />
        )}
      </AdminPage>
    </>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Organizations;
