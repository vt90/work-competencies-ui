import { useEffect } from 'react';
import { withPageAuthRequired } from '../guards/authRequired';

const Unauthenticated = () => {
  useEffect(() => {
    // signOut();
  }, []);
  return 'unauth';
};

export const getServerSideProps = withPageAuthRequired();

export default Unauthenticated;
