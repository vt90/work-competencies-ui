import React, { useState, useMemo, useCallback, useEffect } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { withPageAuthRequired } from '../../../guards/authRequired';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { getCourseById } from '../../../services/courses';
import ListLoadingIndicator from '../../../components/Common/ListLoadingIndcator';
import AdminPage from '../../../components/AdminPage';
import dynamic from 'next/dynamic';
import CourseReviewForm from '../../../components/CourseReview/Form';
import Button from '@mui/lab/LoadingButton';
import { ICourseReview } from '../../../models/courseReview';
import { createOrUpdateCourseReview } from '../../../services/courseReviews';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import { COURSE_VIEWS } from '../../../models/course';
import CourseReviews from '../../../components/Courses/Reviews';
import { getSurveyById, validateQuizAnswers } from '../../../services/surveys';
import CourseTestQA from '../../../components/Courses/CourseTestQA';
import { ISurveyAnswer } from '../../../models/surveyAnswer';
import { createOrUpdateSurveyAnswer } from '../../../services/survey-answers';
import Dialog from '../../../components/Common/Dialog';
import CourseQuestionResponseItem from '../../../components/Courses/CourseQuestionResponseItem';
import Collapse from '@mui/material/Collapse';
import { useAuthContext } from '../../../context/authContext';
import CourseMaterialList from '../../../components/Courses/CourseMaterialList';

const RichText = dynamic(() => import('../../../components/Common/RichText'), {
  ssr: false,
});

const Course = () => {
  const [answerValidation, setAnswerValidation] = useState(undefined);
  const [answeringQuestionId, setAnsweringQuestionId] = useState(undefined);
  const [showReviewForm, setShowReviewForm] = useState(false);
  const { user } = useAuthContext();
  const queryClient = useQueryClient();
  const router = useRouter();
  const { id, activeView } = router.query;

  const FETCH_QUERY_KEY = useMemo(() => ['courses', id], [id]);
  const FETCH_QUERY_SURVEY_KEY = useMemo(() => ['coursesSurvey', id], [id]);

  const { isLoading, data: course } = useQuery(FETCH_QUERY_KEY, () => getCourseById(id as string), {
    onError: () => router.push('/404'),
  });

  const { isLoading: isSurveyLoading, data: courseSurvey } = useQuery(
    FETCH_QUERY_SURVEY_KEY,
    () => getSurveyById(course?.surveyId as string),
    {
      enabled: !!(course && course.surveyId),
      onError: () => router.push('/404'),
    },
  );

  const { mutate: createOrUpdateAnswerMutation } = useMutation(createOrUpdateSurveyAnswer, {
    onSuccess: async () => {
      await queryClient.invalidateQueries(FETCH_QUERY_SURVEY_KEY);
      setAnsweringQuestionId(undefined);
    },
  });

  const { isLoading: isValidating, mutate: validate } = useMutation(validateQuizAnswers, {
    onSuccess: async (data) => {
      setAnswerValidation(data);
    },
  });

  const { isLoading: isReviewLoading, mutate } = useMutation(createOrUpdateCourseReview, {
    onSuccess: async () => {
      await queryClient.invalidateQueries(FETCH_QUERY_KEY);
      setShowReviewForm(false);
    },
  });

  const existingReview = useMemo(() => {
    // @ts-ignore
    return course?.courseReviews?.find((review: ICourseReview) => review.userId === user?._id);
  }, [user, course]);

  const onSubmitReview = (values: Partial<ICourseReview>) => {
    // @ts-ignore
    values.courseId = id;
    // @ts-ignore
    values.userName = user?.name;

    mutate(values);
  };

  const onAnswerChange = (answer: ISurveyAnswer) => {
    // @ts-ignore
    setAnsweringQuestionId(answer?.questionId);
    // @ts-ignore
    answer.userName = user?.name;
    // @ts-ignore
    answer.userEmail = user?.email;
    // @ts-ignore
    answer.userId = user?._id;
    // @ts-ignore
    answer.orgId = courseSurvey?.orgId;

    createOrUpdateAnswerMutation(answer);
  };

  const handleViewChange = useCallback(
    (activeView: string) =>
      router.push({
        pathname: '/courses/[id]',
        query: { id, activeView },
      }),
    [id, router],
  );

  useEffect(() => {
    const existingView = activeView && Object.values(COURSE_VIEWS).find((v) => v === activeView);

    if (!existingView) handleViewChange(COURSE_VIEWS.COURSE);
  }, [activeView, handleViewChange]);

  if (isLoading) return <ListLoadingIndicator />;

  return (
    <div>
      <Head>
        <title>{course?.title}</title>
      </Head>

      {course ? (
        <AdminPage
          actions={
            <>
              {courseSurvey?.published &&
                courseSurvey?.surveyAnswers &&
                courseSurvey?.surveyAnswers?.length === courseSurvey?.questions?.length && (
                  <Collapse
                    in={activeView === COURSE_VIEWS.TESTS}
                    orientation="horizontal"
                    sx={{ maxHeight: '36.5px' }}
                  >
                    <Box>
                      <Button
                        // @ts-ignore
                        onClick={() => validate(courseSurvey?._id)}
                        variant="contained"
                        color="secondary"
                        loading={isValidating}
                        sx={{ mr: 1 }}
                      >
                        Validează răspunsuri
                      </Button>
                    </Box>
                  </Collapse>
                )}
              <Box>
                <Button onClick={() => setShowReviewForm(true)} variant="contained">
                  {existingReview ? 'Modifică' : 'Adaugă'} review
                </Button>
              </Box>
            </>
          }
          pages={[
            { url: '/courses', name: 'Cursuri' },
            // @ts-ignore
            { url: `/courses/${id}`, name: course.title },
          ]}
          title={course.title}
        >
          <Box sx={{ borderBottom: 1, borderColor: 'divider', mb: 4 }}>
            <Tabs value={activeView} onChange={(_, newValue) => handleViewChange(newValue)}>
              {Object.values(COURSE_VIEWS).map((view) => (
                <Tab key={view} value={view} label={view} />
              ))}
            </Tabs>
          </Box>

          {activeView === COURSE_VIEWS.COURSE && (
            <RichText defaultValue={course.content} readOnly />
          )}

          {activeView === COURSE_VIEWS.DOCUMENTS && <CourseMaterialList course={course} />}

          {activeView === COURSE_VIEWS.TESTS && (
            <CourseTestQA
              course={course}
              courseSurvey={courseSurvey}
              isLoading={isSurveyLoading}
              answeringQuestionId={answeringQuestionId}
              onAnswerChange={onAnswerChange}
            />
          )}

          {activeView === COURSE_VIEWS.REVIEWS && (
            <CourseReviews course={course} onSetReview={() => setShowReviewForm(true)} />
          )}
        </AdminPage>
      ) : null}

      {showReviewForm && (
        <CourseReviewForm
          isLoading={isReviewLoading}
          initialValues={existingReview || {}}
          open={showReviewForm}
          onClose={() => setShowReviewForm(false)}
          onSubmit={onSubmitReview}
        />
      )}

      {answerValidation && (
        <Dialog
          content={
            <>
              {/* @ts-ignore */}
              {answerValidation.map((question) => {
                return (
                  <Box key={question._id} sx={{ mb: 4 }}>
                    <CourseQuestionResponseItem question={question} />
                  </Box>
                );
              })}
            </>
          }
          title="Vizualizare rezultate"
          onClose={() => setAnswerValidation(undefined)}
          open={!!answerValidation}
          maxWidth="md"
        />
      )}
    </div>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Course;
