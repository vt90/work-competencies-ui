import React from 'react';
import Head from 'next/head';
import { withPageAuthRequired } from '../../guards/authRequired';
import { useAuthContext } from '../../context/authContext';
import { useQuery } from '@tanstack/react-query';
import { getCourses } from '../../services/courses';
import AdminPage from '../../components/AdminPage';
import ListLoadingIndicator from '../../components/Common/ListLoadingIndcator';
import CoursesList from '../../components/Courses/List';
import NoDataAvailable from '../../components/Common/NoDataAvailable';
import { ORG_ADMIN_PREFIX, ORG_USER_PREFIX } from '../../models/user';

const FETCH_QUERY_KEY = ['courses'];

const Courses = () => {
  const { organization } = useAuthContext();

  const { isLoading, data: courses } = useQuery(
    FETCH_QUERY_KEY,
    () =>
      getCourses({
        orgId: organization.replace(ORG_USER_PREFIX, ORG_ADMIN_PREFIX),
      }),
    {
      enabled: !!organization,
    },
  );

  return (
    <div>
      <Head>
        <title>Cursuri</title>
      </Head>

      <AdminPage title="Cursuri" pages={[{ url: '/courses', name: 'Cursuri' }]}>
        {courses?.length || isLoading ? (
          <>
            {isLoading && <ListLoadingIndicator />}

            {courses && <CoursesList baseUrl="/courses" courses={courses} readonly />}
          </>
        ) : (
          <NoDataAvailable content="În momentul de faţă nu există niciun curs disponibil pe platformă" />
        )}
      </AdminPage>
    </div>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default Courses;
