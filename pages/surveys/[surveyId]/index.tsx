import React, { useState, useMemo } from 'react';
import Head from 'next/head';
import { withPageAuthRequired } from '../../../guards/authRequired';
import { useRouter } from 'next/router';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { getSurveyById } from '../../../services/surveys';
import ListLoadingIndicator from '../../../components/Common/ListLoadingIndcator';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import QuestionsResponseList from '../../../components/Questions/ResponseList';
import { createOrUpdateSurveyAnswer } from '../../../services/survey-answers';
import { ISurveyAnswer } from '../../../models/surveyAnswer';
import AdminPage from '../../../components/AdminPage';
import PercentBar from '../../../components/Common/Charts/PercentBar';
import { COLORS } from '../../../lib/theme';
import { useAuthContext } from '../../../context/authContext';

const SurveySession = () => {
  const [answeringQuestionId, setAnsweringQuestionId] = useState(undefined);
  const router = useRouter();
  const { surveyId } = router.query;

  const { user } = useAuthContext();
  const queryClient = useQueryClient();

  const FETCH_QUERY_KEY = useMemo(() => ['surveys', surveyId], [surveyId]);

  const { isLoading: isSurveyLoading, data: survey } = useQuery(
    FETCH_QUERY_KEY,
    () => getSurveyById(surveyId as string),
    {
      onError: () => router.push('/404'),
    },
  );

  const { mutate: createOrUpdateAnswerMutation } = useMutation(createOrUpdateSurveyAnswer, {
    onSuccess: async () => {
      await queryClient.invalidateQueries(FETCH_QUERY_KEY);
      setAnsweringQuestionId(undefined);
    },
  });

  const onAnswerChange = (answer: ISurveyAnswer) => {
    // @ts-ignore
    setAnsweringQuestionId(answer?.questionId);
    // @ts-ignore
    answer.userName = user?.name;
    // @ts-ignore
    answer.userEmail = user?.email;
    // @ts-ignore
    answer.userId = user?._id;
    // @ts-ignore
    answer.orgId = survey?.orgId;

    createOrUpdateAnswerMutation(answer);
  };

  if (isSurveyLoading) return <ListLoadingIndicator />;

  const progress =
    (survey?.surveyAnswers?.length &&
      survey?.questions?.length &&
      (survey?.surveyAnswers?.length / survey?.questions?.length) * 100) ||
    0;

  return (
    <div>
      <Head>
        <title>{survey?.name}</title>
      </Head>

      <AdminPage
        actions={
          <Box sx={{ width: '190px', maxWidth: '70vw' }}>
            <PercentBar color={COLORS[0]} label={'Progress'} value={progress} />
          </Box>
        }
        title={`${survey?.name}`}
        pages={[
          { url: '/surveys', name: 'Chestionare' },
          { url: `/surveys/${surveyId}`, name: `${survey?.name}` },
        ]}
      >
        {survey ? (
          <>
            <Box mb={6}>
              <Typography>{survey.description}</Typography>
            </Box>

            <QuestionsResponseList
              answeringQuestionId={answeringQuestionId}
              // @ts-ignore
              questions={survey.questions}
              // @ts-ignore
              surveyAnswers={survey?.surveyAnswers}
              onAnswerChange={onAnswerChange}
            />
          </>
        ) : (
          // ToDo add warning here
          'Internal Server error'
        )}
      </AdminPage>
    </div>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default SurveySession;
