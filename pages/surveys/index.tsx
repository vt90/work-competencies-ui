import React from 'react';
import Head from 'next/head';
import { withPageAuthRequired } from '../../guards/authRequired';
import { useQuery } from '@tanstack/react-query';
import AdminPage from '../../components/AdminPage';
import ListLoadingIndicator from '../../components/Common/ListLoadingIndcator';
import SurveysList from '../../components/Surveys/List';
import { useAuthContext } from '../../context/authContext';
import { ORG_ADMIN_PREFIX, ORG_USER_PREFIX } from '../../models/user';
import { getSurveys } from '../../services/surveys';
import NoDataAvailable from '../../components/Common/NoDataAvailable';

const FETCH_QUERY_KEY = ['surveys'];

const SurveySessions = () => {
  const { organization } = useAuthContext();

  const { isLoading, data: surveys } = useQuery(
    FETCH_QUERY_KEY,
    () =>
      getSurveys({
        orgId: organization.replace(ORG_USER_PREFIX, ORG_ADMIN_PREFIX),
      }),
    {
      enabled: !!organization,
    },
  );

  return (
    <div>
      <Head>
        <title>Chestionare</title>
      </Head>

      <AdminPage title="Chestionare" pages={[{ url: '/surveys', name: 'Chestionare' }]}>
        {surveys?.length || isLoading ? (
          <>
            {isLoading && <ListLoadingIndicator />}

            {surveys && <SurveysList baseUrl="/surveys" surveys={surveys} readonly />}
          </>
        ) : (
          <NoDataAvailable content="În momentul de faţă nu există niciun chestionar disponibil pe platformă" />
        )}
      </AdminPage>
    </div>
  );
};

export const getServerSideProps = withPageAuthRequired();

export default SurveySessions;
