import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import { useAuthContext } from '../context/authContext';
import { USER_PAGES } from '../models/userPages';
import Error from 'next/error';
import { withPageAuthRequired } from '../guards/authRequired';
import CMSPage from '../components/CMS/page';

const Home = () => {
  const { user, organization } = useAuthContext();
  const router = useRouter();

  const isPublicFacing = !!process.env.NEXT_PUBLIC_PUBLIC_FACING_WEBSITE;

  useEffect(() => {
    const firstAvailablePage =
      organization && USER_PAGES.find((page) => page.isAvailable(organization));

    if (!isPublicFacing && firstAvailablePage) {
      router.push(firstAvailablePage.url);
    }
  }, [organization, router, isPublicFacing]);

  // @ts-ignore
  if (user && !user?.userRoles?.length && !isPublicFacing) {
    return (
      <Box
        sx={{
          '& > div': {
            border: '1px solid white',
            maxHeight: 'calc(100vh - 180px)',
          },
        }}
      >
        <Error
          statusCode={403}
          title="Contactati administratorul pentru a va asigna organizatiei"
        />
      </Box>
    );
  }

  if (isPublicFacing) {
    return <CMSPage />;
  }

  return (
    <>
      <LinearProgress sx={{ marginTop: -4 }} />
    </>
  );
};

export const getServerSideProps = !!process.env.NEXT_PUBLIC_PUBLIC_FACING_WEBSITE
  ? () => ({ props: {} })
  : () => withPageAuthRequired();

export default Home;
