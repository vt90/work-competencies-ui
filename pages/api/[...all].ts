import { NextApiRequest, NextApiResponse } from 'next';
import axios from 'axios';
import has from 'lodash/has';
import { withApiAuthRequired } from '../../guards/authRequired';
import { getServerSession } from 'next-auth/next';
import { authOptions } from './auth/[...nextauth]';

export default withApiAuthRequired(async function (req: NextApiRequest, res: NextApiResponse) {
  const session = await getServerSession(
    // @ts-ignore
    req,
    res,
    authOptions,
  );

  try {
    const reqOptions = {
      ...req,
      method: req.method,
      baseURL: process.env.NEXT_PUBLIC_API_URL,
      url: req.url,
      data: req.body,
      headers: {
        // @ts-ignore
        Authorization: `Bearer ${session?.accessToken}`,
      },
    };

    if (has(req.body, '_id')) delete req.body._id;
    if (has(req.body, '__v')) delete req.body.__v;
    if (has(req.body, 'createdAt')) delete req.body.createdAt;
    if (has(req.body, 'updatedAt')) delete req.body.updatedAt;

    // @ts-ignore
    return axios(reqOptions)
      .then((response) => {
        res.status(response.status).json(response.data);
      })
      .catch((error) => {
        res.status(error.response.status).json(error.response.data);
      });
  } catch ({ error, message }) {
    // if the access token and refresh token is invalid
    if (error === 'invalid_grant') {
      res.status(401).json({
        error: 'not_authenticated',
        description: 'The user does not have an accessToken',
      });
    } else {
      res.status(500).end(message);
    }
    return;
  }
});
