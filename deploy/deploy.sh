#!/bin/bash

DEPLOY_SERVER=89.45.251.204

echo "Deploying to ${DEPLOY_SERVER}"
ssh chainnodes@${DEPLOY_SERVER} 'bash' < ./deploy/server.sh
