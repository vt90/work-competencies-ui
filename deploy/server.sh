# Pull code
cd ./projects/work-competencies/work-competencies-ui
pm2 stop work-comp-ui
git pull ${CI_REPOSITORY_URL}

# Build and deploy
yarn install
ls -la
pm2 start work-comp-ui-pm2.json
echo DONE
