import { isOrgAdmin, isOrgUser, isSuperAdmin } from './user';

export interface IMenuPage {
  name: string;
  description: string;
  url: string;
  isAvailable: (params?: any) => boolean;
}

export const USER_PAGES: IMenuPage[] = [
  {
    name: 'Pagina principală',
    url: '/',
    description: 'Pagina principală',
    isAvailable: (org: string) =>
      isSuperAdmin(org) && !!process.env.NEXT_PUBLIC_PUBLIC_FACING_WEBSITE,
  },
  {
    name: 'Organizații',
    url: '/management/organizations',
    description: 'Vizualizează toate organizaţiile din platforma',
    isAvailable: (org: string) => isSuperAdmin(org),
  },
  {
    name: 'Chestionare',
    url: '/management/surveys',
    description: 'Gestionează chestionarele organizaţiei',
    isAvailable: (org) => isOrgAdmin(org),
  },
  {
    name: 'Cursuri',
    url: '/management/courses',
    description: 'Gestionează materialele didactice oferite de organizaţie',
    isAvailable: (org) => isOrgAdmin(org),
  },
  {
    name: 'Utilizatori',
    url: '/management/users',
    description: 'Gestionează utilizatorii si accesul lor',
    isAvailable: (org) => isOrgAdmin(org),
  },
  {
    name: 'Chestionare',
    url: '/surveys',
    description: 'Participă la chestionarele organizaţiei',
    isAvailable: (org) => isOrgUser(org),
  },
  {
    name: 'Cursuri',
    url: '/courses',
    description: 'Consultă cursurile oferite',
    isAvailable: (org) => isOrgUser(org),
  },
];
