import { IQuestion } from './question';
import { IBaseModel } from './base';
import { ISurveyAnswer } from './surveyAnswer';

export interface ISurvey extends IBaseModel {
  name?: string;
  description?: string;
  published?: boolean;
  type?: number;
  questions?: IQuestion[];
  surveyAnswers?: ISurveyAnswer[];
}

export const SURVEY_VIEWS = {
  STRUCT: 'Structura',
  RESULTS: 'Statistici',
  INDIVIDUALS: 'Rezultate individuale',
};
