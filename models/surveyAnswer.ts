import { IBaseModel } from './base';

export interface ISurveyAnswer extends IBaseModel {
  surveyId?: string;
  questionId?: string;
  userId?: string;
  userName?: string;
  userEmail?: string;
  answer?: string;
  displayResult?: string;
  result?: number;
}

export interface ISurveyAnswerWithProgress extends ISurveyAnswer {
  userAnsweredQuestions: number;
}

export interface IGroupedSurveyAnswers {
  [key: string]: ISurveyAnswer[];
}
