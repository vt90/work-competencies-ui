import { IBaseModel } from './base';

export interface ICmsSectionFile extends IBaseModel {
  name: string;
  originalName: string;
  url: string;
  fileType: string;
}

export interface ICmsSection extends IBaseModel {
  title?: string;
  description?: string;
  files?: ICmsSectionFile[];
  uploadFiles?: any[];
}
