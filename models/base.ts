export interface IBaseModel {
    _id?: string;
    orgId?: string;
    createdAt?: string;
    updatedAt?: string;
}
