import {IBaseModel} from "./base";

export interface IQuestionAnswer {
    _id?: string;
    text?: string;
    isCorrectAnswer?: boolean;
}

export interface IQuestion extends IBaseModel {
    surveyId?: string;
    text?: string;
    includeOtherAsOption?: boolean;
    type?: number;
    answers?: IQuestionAnswer[];
}
