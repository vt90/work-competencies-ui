import { IBaseModel } from './base';

export interface ICourseReview extends IBaseModel {
  courseId?: string;
  userId?: string;
  userName?: string;
  review?: string;
  rating?: number;
}
