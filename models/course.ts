import { IBaseModel } from './base';
import { ICourseReview } from './courseReview';
import { ISurvey } from './survey';

export interface ICourse extends IBaseModel {
  title?: string;
  slug?: string;
  content?: string;
  published?: boolean;
  courseReviews?: ICourseReview[];
  courseMaterials?: ICourseMaterial[];
  avgReview?: number;
  surveyId?: string;
  courseSurvey?: ISurvey;
}

export interface ICourseMaterialFile extends IBaseModel {
  name: string;
  originalName: string;
  url: string;
  fileType: string;
}

export interface ICourseMaterial extends IBaseModel {
  title?: string;
  description?: string;
  files?: ICourseMaterialFile[];
  uploadFiles?: any[];
}

export const COURSE_VIEWS = {
  COURSE: 'Descriere',
  DOCUMENTS: 'Materiale Curs',
  TESTS: 'Evaluare',
  REVIEWS: 'Feedback',
};
