import { IBaseModel } from './base';

export interface IOrganization extends IBaseModel {
  name: string;
  tenant?: string;
}

export interface IOrganizationUser {
  organizationId: string;
  role: string;
  email: string;
  name: string;
  tenant?: string;
  isAdmin?: boolean;
}
