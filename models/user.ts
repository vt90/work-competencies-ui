export interface IUserRole {
  _id: string;
  name: string;
}
export interface IUser {
  user_id: string;
  name: string;
  email: string;
  picture?: string;
  userRoles?: IUserRole[];
}

export const SUPER_ADMIN_ROLE = 'SUPER_ADMIN';
export const ORG_ADMIN_PREFIX = 'ADMIN_';
export const ORG_USER_PREFIX = 'USER_';

export const isSuperAdmin = (role: string): boolean => role === SUPER_ADMIN_ROLE;
export const isOrgAdmin = (role: string): boolean => role?.startsWith(ORG_ADMIN_PREFIX);
export const isOrgUser = (role: string): boolean => role?.startsWith(ORG_USER_PREFIX);
